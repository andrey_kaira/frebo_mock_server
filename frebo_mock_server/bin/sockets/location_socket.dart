import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart' as http;

import '../functions/shared/check_param.dart';
import '../functions/shared/check_token_function.dart';
import '../res/functions_key.dart';

class LocationSocket {
  static ServerSocket _locationSocket;

  static Future<void> init({String ip, int port}) async {
    _locationSocket = await ServerSocket.bind(ip, port);
    await _locationSocket.listen((client) => _handleConnection(client));
    print('LocationSocket start $ip:$port');
  }

  static void _handleConnection(Socket client) {
    try {
      print('Connection from' ' ${client.remoteAddress.address}:${client.remotePort}');
      client.listen(
        (Uint8List data) async {
          try {
            final message = Utf8Codec().decode(data);
            final body = jsonDecode(message);
            await Future.delayed(Duration(milliseconds: 1000));
            final token = body[AuthKeys.token];
            final value = body[LocationKeys.value];

            final checkToken = await checkTokenFunction(token);
            if (checkToken != null) {
              client.write(jsonEncode([]));
              return;
            }

            if (checkParamIsEmpty(value)) {
              client.write(jsonEncode([]));
              return;
            }

            final response = await _searchRequest(value.toString().replaceAll(' ', '-'));
            final dataAddress = jsonDecode(response)['features'];

            if (dataAddress.isEmpty) {
              client.write(jsonEncode([]));
              return;
            }
            final addressList = (dataAddress as List<dynamic>).map((e) => {'text': e['text'], 'center': e['center']}).toList();

            client.write(jsonEncode(addressList));
          } catch (error) {
            print(error);
          }
        },
        onError: (error) {
          try {
            print('$error');
            client.close();
            client.destroy();
          } catch (error) {
            print('Error? $error');
          }
        },
        onDone: () {
          try {
            print('${client.address.address}:${client.remotePort} disconnect! (Done)');
            client.close();
          } catch (error) {
            print(error);
          }
        },
        cancelOnError: true,
      );
    } catch (error) {
      print(error);
    }
  }

  static Future<String> _searchRequest(String value) async {
    try {
      print(value);
      var request = http.Request(
          'GET',
          Uri.parse(
              'https://api.mapbox.com/geocoding/v5/mapbox.places/${value}.json?limit=6&access_token=pk.eyJ1IjoiYW5kcmV5a2FpcmEiLCJhIjoiY2tsdXZ6NDJzMmFoODJ2cW1tOGI1emt2aiJ9.ujVxmGaziesE_KfOLKy49w'));
      request.body = '''''';

      final response = await request.send();

      return await response.stream.bytesToString();
    } catch (error) {
      print(error);
      return null;
    }
  }
}
