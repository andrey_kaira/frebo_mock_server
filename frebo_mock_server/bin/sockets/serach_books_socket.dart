import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import '../functions/shared/check_param.dart';
import '../functions/shared/check_token_function.dart';
import '../models/search_model.dart';
import '../requests/local_requests/books_local_requests.dart';
import '../res/functions_key.dart';

class SearchBooksSocket {
  static ServerSocket _locationSocket;
  static Future<void> init({String ip, int port}) async {
    _locationSocket = await ServerSocket.bind(ip, port);
    await _locationSocket.listen((client) => _handleConnection(client));
    print('ServerSocket start $ip:$port');
  }
  static void _handleConnection(Socket client) {
    try {
      print('Connection from' ' ${client.remoteAddress.address}:${client.remotePort}');
      client.listen(
        (Uint8List data) async {
          try {
            final message = Utf8Codec().decode(data);
            final body = jsonDecode(message);
            final token = body[AuthKeys.token];
            final value = body[LocationKeys.value]
                .toString()
                .toLowerCase()
                .replaceAll(':', '')
                .replaceAll('.', '')
                .replaceAll('&', '')
                .replaceAll('/', '')
                .replaceAll(',', '')
                .replaceAll('-', '')
                .replaceAll('\\', '')
                .replaceAll('+', '')
                .replaceAll('*', '')
                .replaceAll('"', '')
                .replaceAll("'", '');
            final mode = body[LocationKeys.mode];
            final values = value.split(' ');
            print(values);
            final checkToken = await checkTokenFunction(token);
            if (checkToken != null) {
              client.write(jsonEncode([]));
              return;
            }
            if (checkParamIsEmpty(value)) {
              client.write(jsonEncode([]));
              return;
            }
            final searchModels = <SearchModel>[];
            final books = await BooksLocalStoreRequest.getBooks();
            values.forEach((element) {
              books.forEach((book) {
                final names = book.name
                    .toLowerCase()
                    .replaceAll(':', '')
                    .replaceAll('.', '')
                    .replaceAll(',', '')
                    .replaceAll('&', '')
                    .replaceAll('/', '')
                    .replaceAll('\\', '')
                    .replaceAll('-', '')
                    .replaceAll('+', '')
                    .replaceAll('*', '')
                    .replaceAll('"', '')
                    .replaceAll("'", '')
                    .split(' ');
                if (names.contains(element) || book.writer.toLowerCase().split(' ').contains(element)) {
                  if (searchModels.indexWhere((element) => element.book.id == book.id) == -1) {
                    searchModels.add(SearchModel(book: book));
                  } else {
                    searchModels.firstWhere((element) => element.book.id == book.id).count++;
                  }
                }
              });
            });
            searchModels.sort((a, b) => a.count > b.count ? 0 : 1);
            searchModels.removeWhere((element) => element.count < values.length - 1);
            if (mode == ModeSearch.find) {
              searchModels.sort((a, b) => a.book.distance < b.book.distance ? 0 : 1);
            }
            client.write(jsonEncode(searchModels.map((e) => e.book.toJson()).toList()));
          } catch (error) {
            print(error);
          }
        },
        onError: (error) {
          try {
            print('$error');
            client.close();
            client.destroy();
          } catch (error) {
            print('Error? $error');
          }
        },
        onDone: () {
          try {
            print('${client.address.address}:${client.remotePort} disconnect! (Done)');
            client.close();
          } catch (error) {
            print(error);
          }
        },
        cancelOnError: true,
      );
    } catch (error) {
      print(error);
    }
  }
}

class ModeSearch {
  static const String library = 'library';
  static const String find = 'find';
}
