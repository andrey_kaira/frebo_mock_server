import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_key.dart';
import '../shared/not_found_functions.dart';
import 'functions/add_book_library_function.dart';
import 'functions/get_library_books.dart';


class LibraryManager {
  static Future<shelf.Response> libraryFunctionsManager(String path, {Map<String, String> headers, Map<String, dynamic> body}) async {
    switch (path) {
      case FunctionsKeys.addBookLibrary:
        return await addBookLibraryFunction(headers: headers, body: body);
      case FunctionsKeys.getLibraryBooks:
        return await getLibraryBooksFunction(headers: headers);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
