import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> addBookLibraryFunction({Map<String, String> headers, Map<String, dynamic> body}) async {
  final token = headers[AuthKeys.token];
  final bookId = body[BooksKeys.bookId];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  if (checkParamIsEmpty(bookId.toString())) return Response(400, body: 'Books id is empty');

  final books = await BooksLocalStoreRequest.getBooks();
  final book = books.firstWhere((element) => element.id == bookId, orElse: () => null);
  if (book == null) return Response(404, body: 'Books not found!');

  final currentToken = await AuthLocalStoreRequest.getToken(token);
  currentToken.user.library.books.add(book);

  AuthLocalStoreRequest.updateUser(currentToken.user);
  AuthLocalStoreRequest.updateTokenUser(currentToken.user);

  final res = jsonEncode({'status': 'Ok!'});
  return Response.ok(res, headers: CurrentHeaders.getHeaders());
}
