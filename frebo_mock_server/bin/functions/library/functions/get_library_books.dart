import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> getLibraryBooksFunction({Map<String, String> headers}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  final res = jsonEncode({'data': currentToken.user.library.toJson()});
  return Response.ok(res, headers: CurrentHeaders.getHeaders());
}
