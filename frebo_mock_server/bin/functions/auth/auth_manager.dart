import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_key.dart';
import '../shared/not_found_functions.dart';
import 'functions/facebook_join_functions.dart';
import 'functions/logout.dart';
import 'functions/select_info_functions.dart';

class AuthManager {
  static Future<shelf.Response> authFunctionsManager(String path, {Map<String, String> headers, Map<String, dynamic> body}) async {
    switch (path) {
      case FunctionsKeys.facebookAuth:
        return await facebookJoinFunctions(body: body);
      case FunctionsKeys.selectInfo:
        return await selectInfo(headers: headers, body: body);
      case FunctionsKeys.logout:
        return await logout(headers: headers);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
