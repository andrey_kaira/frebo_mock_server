import 'dart:convert';

import 'package:shelf/shelf.dart';
import 'package:uuid/uuid.dart';

import '../../../models/library.dart';
import '../../../models/token.dart';
import '../../../models/user.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../shared/check_param.dart';
import '../../shared/current_headers.dart';

Future<Response> facebookJoinFunctions({Map<String, dynamic> body}) async {
  try {
    final user = User.fromJson(body);
    if (checkParamIsEmpty(user.email)) return Response(400, body: 'Email is empty');
    if (checkParamIsEmpty(user.firstName)) return Response(400, body: 'Name is empty');

    user.library ??= Library(books: []);

    var currentUser = await AuthLocalStoreRequest.getUser(user.email);
    currentUser ??= await AuthLocalStoreRequest.saveNewUser(user);

    final token = Token(
      user: currentUser,
      token: Uuid().v4(),
    );

    AuthLocalStoreRequest.saveNewToken(token);

    return Response.ok(jsonEncode(currentUser.toJsonResponse(token)), headers: CurrentHeaders.getHeaders());
  } catch (e) {
    print(e);
    return Response(400, body: 'User, was transmitted incorrectly!');
  }
}
