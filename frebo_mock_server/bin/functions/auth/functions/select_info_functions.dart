import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/registartion_model.dart';
import '../../../models/user.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> selectInfo({Map<String, String> headers, Map<String, dynamic> body}) async {
  try {
    final registrationModel = RegistrationModel.fromJson(body);
    final token = headers[AuthKeys.token];

    final checkToken = await checkTokenFunction(token);
    if (checkToken != null) return checkToken;
    //if (checkParamIsEmpty(registrationModel.gender)) return Response(400, body: 'Gender is empty');
    //if (checkParamIsEmpty(registrationModel.address)) return Response(400, body: 'Address is empty');
    //if (checkParamIsEmpty(registrationModel.birthday?.toString())) return Response(400, body: 'Birthday is empty');
    //if (registrationModel.cords.isEmpty) return Response(400, body: 'Cords is empty');

    final currentToken = await AuthLocalStoreRequest.getToken(token);
    final user = User.insertData(user: currentToken.user, registrationModel: registrationModel);
    AuthLocalStoreRequest.updateUser(user);
    AuthLocalStoreRequest.updateTokenUser(user);

    return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
  } catch (e) {
    print(e);
    return Response(400, body: 'User, was transmitted incorrectly!');
  }
}
