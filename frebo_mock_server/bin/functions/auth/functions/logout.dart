import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> logout({Map<String, String> headers, Map<String, dynamic> body}) async {
  try {
    final token = headers[AuthKeys.token];

    final checkToken = await checkTokenFunction(token);
    if (checkToken != null) return checkToken;

    final currentToken = await AuthLocalStoreRequest.getToken(token);

    AuthLocalStoreRequest.removeTokenUser(currentToken);

    return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
  } catch (e) {
    print(e);
    return Response(400, body: 'User, was transmitted incorrectly!');
  }
}
