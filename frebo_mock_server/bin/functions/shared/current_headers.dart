class CurrentHeaders {
  static Map<String, String> getHeaders({String contentType}) {
    return {
      'Content-Type': contentType ?? 'application/json',
    };
  }
}
