bool checkParamIsEmpty(String param) {
  return param == null || param == '';
}
