import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_key.dart';
import '../shared/not_found_functions.dart';
import 'functions/users_functions.dart';

class UsersManager {
  static Future<shelf.Response> usersFunctionsManager(String path, {String method, Map<String, String> headers, Map<String, dynamic> body}) async {
    switch (path) {
      case FunctionsKeys.users:
        return await usersFunctions(headers: headers, body: body, method: method);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
