import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> recommendedBooksFunction({Map<String, String> headers}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final books = await BooksLocalStoreRequest.getBooks();
  books.sort((a, b) => a.distance < b.distance ? 0 : 1);
  final res = jsonEncode(books.map((e) => e.toJson()).toList());
  return Response.ok(res, headers: CurrentHeaders.getHeaders());
}
