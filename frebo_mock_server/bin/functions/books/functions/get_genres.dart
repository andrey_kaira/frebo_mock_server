import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../res/dummy_data.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> getGenresFunction({Map<String, String> headers}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final res = jsonEncode(dummy_genre.map((e) => e.toJson()).toList());
  return Response.ok(res, headers: CurrentHeaders.getHeaders());
}