import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/friend.dart';
import '../../../models/review.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_key.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> addReviewFunction({Map<String, String> headers, Map<String, dynamic> body}) async {
  final token = headers[AuthKeys.token];
  final bookId = headers[BooksKeys.bookId];
  final review = Review.fromJson(body);

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  if (checkParamIsEmpty(bookId)) return Response(400, body: 'Books id is empty');
  if (review?.rating == null) return Response(400, body: 'Rating is empty');

  final currentToken = await AuthLocalStoreRequest.getToken(token);
  review.friend = Friend(
    id: currentToken.user.id,
    locationLat: currentToken.user.locationLat,
    locationLng: currentToken.user.locationLng,
    name: currentToken.user.firstName + ' ' + currentToken.user.lastName,
  );

  //final res = await BooksLocalStoreRequest.containReviewBooks(bookId: bookId, userId: currentToken.user.id);
  //if (res) return Response(403, body: 'You can only leave one comment!');
  await BooksLocalStoreRequest.addReviewBooks(bookId: bookId, review: review);

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
}
