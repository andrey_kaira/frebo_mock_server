import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_key.dart';
import '../shared/not_found_functions.dart';
import 'functions/add_review_function.dart';
import 'functions/recommended_books_function.dart';
import 'functions/get_genres.dart';

class BooksManager {
  static Future<shelf.Response> booksFunctionsManager(String path, {Map<String, String> headers, Map<String, dynamic> body}) async {
    switch (path) {
      case FunctionsKeys.recommendedBooks:
        return await recommendedBooksFunction(headers: headers);
      case FunctionsKeys.addReview:
        return await addReviewFunction(headers: headers, body: body);
      case FunctionsKeys.getGenres:
        return await getGenresFunction(headers: headers);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
