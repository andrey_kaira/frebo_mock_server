import 'package:uuid/uuid.dart';

import '../models/book.dart';
import '../models/genre.dart';

List<Genre> dummy_genre = [
  Genre(id: Uuid().v4().substring(0, 8), name: 'Adult Psychology', desc: 'Desc!'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Anatomy & Physiology'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Medical Anatomy'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'City Life Fiction'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Urban Fiction'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Pop Artist Biographies'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Popular Music (Books)'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Motherhood'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Biographies of Women'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Christian Thriller & Suspense'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Christian Mystery & Suspense'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Contemporary Romance', desc: 'Desc!'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Military Romance'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Contemporary Romance', desc: 'Desc!'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Regency Romance'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Poetry & Nursery Rhymes for Children'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Holidays & Celebrations for Children'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Non-religious Holiday Books', desc: 'Desc!'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Baby Animal Books'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Parents Books', desc: 'Desc!'),
  Genre(id: Uuid().v4().substring(0, 8), name: ''),
];

List<Book> dummy_books = <Book>[
  Book(
      id: Uuid().v4().substring(0, 8),
      name: 'The Body: A Guide for Occupants',
      description: '#1 Bestseller in both hardback and paperback: SHORTLISTED FOR THE 2020 ROYAL SOCIETY INSIGHT INVESTMENT SCIENCE BOOK PRIZE',
      distance: 10,
      writer: 'Bill Bryson',
      image: 'https://m.media-amazon.com/images/I/51LMZBNahWL.jpg',
      reviews: [],
      friends: [],
      genres: [
        Genre(id: Uuid().v4().substring(0, 8), name: 'Adult Psychology'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Anatomy & Physiology'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Medical Anatomy'),
      ]),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Saint X: A Novel',
    description:
        'A New York Times Notable Book of 2020\n\n\"\'Saint X\' is hypnotic. Schaitkin\'s characters...are so intelligent and distinctive it feels not just easy, but necessary, to follow them. I devoured [it] in a day."–Oyinkan Braithwaite, New York Times Book Review',
    distance: 15,
    writer: 'Alexis Schaitkin',
    image: 'https://m.media-amazon.com/images/I/51pMrW0+vcL.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'City Life Fiction'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Urban Fiction'),
    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Me: Elton John Official Autobiography',
    description:
        'INSTANT #1 NEW YORK TIMES BESTSELLER\n\nIn his first and only official autobiography, music icon Elton John reveals the truth about his extraordinary life, from his rollercoaster lifestyle as shown in the film Rocketman, to becoming a living legend.',
    distance: 20,
    writer: 'Elton John',
    image: 'https://m.media-amazon.com/images/I/51y8+fZ4bcL.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Pop Artist Biographies'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Popular Music (Books)', desc: 'Desc!'),
    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Wild Game: My Mother, Her Secret, and Me',
    description:
        'Great Experience. Great Value.\nEnjoy a great reading experience when you buy the Kindle edition of this book. Learn more about Great on Kindle, available in select categories.',
    distance: 2,
    writer: 'Adrienne Brodeur',
    image: 'https://m.media-amazon.com/images/I/413kTDJpUKL.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Motherhood'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Biographies of Women'),
    ],
  ),
  Book(
      id: Uuid().v4().substring(0, 8),
      name: 'Prosper\'s Demon',
      description:
          '"As if Deadpool had slipped into the body of the Witcher Geralt." —The New York Times\n\nIn the pitch dark, witty fantasy novella Prosper\'s Demon, K. J. Parker deftly creates a world with vivid, unbending rules, seething with demons, broken faith, and worse men.',
      distance: 10,
      writer: 'K. J. Parker',
      image: 'https://m.media-amazon.com/images/I/41b86Pk3JkL.jpg',
      reviews: [],
      friends: [],
      genres: [
        Genre(id: Uuid().v4().substring(0, 8), name: 'Christian Thriller & Suspense'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Christian Mystery & Suspense', desc: 'Desc!'),
      ]),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Tucker: Eternity Springs: The McBrides of Texas',
    description:
        'In Eternity Springs: The McBrides of Texas, New York Times bestselling author Emily March presents a brand new arc set in the Lone Star State that features a family-linked trilogy within the author\'s romantic series.',
    distance: 100,
    writer: 'Emily March',
    image: 'https://m.media-amazon.com/images/I/51yxyi4FQ7L.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Contemporary Romance'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Military Romance'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Contemporary Romance', desc: 'Desc!'),
    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Forever My Duke: Unlikely Duchesses',
    description:
        'Forever My Duke is the second novel in a brand new Regency romance series from Olivia Drake about rakish dukes and the governesses who steal their hearts.“I find Miss Fanshawe to be quite charming—for an American.”—The Prince Regent',
    distance: 25,
    writer: 'Olivia Drake',
    image: 'https://m.media-amazon.com/images/I/51200dkZwOL.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Regency Romance'),
    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'How to Catch a Leprechaun',
    description:
        'The #1 New York Times and USA Today bestseller perfect for St. Patrick\'s Day and beyond!\n\nYou\'ve been planning night and day, and finally you\'ve created the perfect trap with shamrocks, pots of gold, and rainbows galore! Now all you need to do is wait. Is this the year you\'ll finally catch the leprechaun? Start a St. Patrick\'s Day tradition with this fun and lively children\'s picture book and get inspired to build leprechaun traps of your own at home or in the classroom! Laugh along in this zany story for kids that blends STEAM concepts with hilarious rhymes and vibrant illustrations!',
    distance: 90,
    writer: 'Adam Wallace',
    image: 'https://images-na.ssl-images-amazon.com/images/I/61DY-xNKbGL._SY498_BO1,204,203,200_.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Poetry & Nursery Rhymes for Children'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Holidays & Celebrations for Children'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Non-religious Holiday Books'),
    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Earth: By The Numbers',
    description:
        'Caldecott Honor winner Steve Jenkins introduces By the Numbers infographic readers chock full of incredible infographs and stunning, full-color cut-paper illustrations. Earth will focus on the fascinating ins-and-outs of earth science.',
    distance: 20,
    writer: 'Steve Jenkins',
    image: 'https://m.media-amazon.com/images/I/41Ay5Pxt2cL.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'City Life Fiction'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Holidays & Celebrations for Children'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Non-religious Holiday Books'),
    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'If Animals Kissed Good Night',
    description:
        'Don\'t miss the other books in this adorable series: If Animals Said I Love You, If Animals Celebrated Christmas, If Animals Went to School, and If Animals Gave Thanks!',
    distance: 150,
    writer: 'Ann Whitford Paul ',
    image: 'https://images-na.ssl-images-amazon.com/images/I/51PRQuO-xjL._SY498_BO1,204,203,200_.jpg',
    reviews: [],
    friends: [],
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Motherhood'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Baby Animal Books'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Parents Books'),
    ]
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Baby Sign Language Made Easy: 101 Signs to Start Communicating with Your Child Now',
    description: 'The easy way to teach sign language to babies and toddlers ages 0 to 3',
    distance: 2,
    writer: 'Lane Rebelo',
    image: 'https://images-na.ssl-images-amazon.com/images/I/51WHRpkDS4L._SX474_BO1,204,203,200_.jpg',
    reviews: [],
    friends: [],
    genres: [

    ],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name:
        'The Unofficial Disney Parks Cookbook: From Delicious Dole Whip to Tasty Mickey Pretzels, 100 Magical Disney-Inspired Recipes (Unofficial Cookbook) ',
    description:
        'Great Experience. Great Value.\nEnjoy a great reading experience when you buy the Kindle edition of this book. Learn more about Great on Kindle, available in select categories.',
    distance: 10,
    writer: 'Ashley Craft',
    image: 'https://images-na.ssl-images-amazon.com/images/I/51QBFxYP2tL._SX382_BO1,204,203,200_.jpg',
    reviews: [],
    friends: [],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'Life After Death: A Novel',
    description: 'The long-anticipated sequel to Sister Souljah’s million copy bestseller The Coldest Winter Ever.',
    distance: 10,
    writer: 'Sister Souljah ',
    image: 'https://images-na.ssl-images-amazon.com/images/I/41FYi16jzwL._SX327_BO1,204,203,200_.jpg',
    reviews: [],
    friends: [],
  ),
  Book(
    id: Uuid().v4().substring(0, 8),
    name: 'A Promised Land',
    description:
        'A riveting, deeply personal account of history in the making—from the president who inspired us to believe in the power of democracy',
    distance: 200,
    writer: 'Barack Obama',
    image: 'https://sun9-30.userapi.com/c844616/v844616011/f02dc/40k1XZ-4M8g.jpg',
    reviews: [],
    friends: [],
  ),
];
