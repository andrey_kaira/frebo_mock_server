class FunctionsKeys {
  //Auth functions
  static const String facebookAuth = 'facebookAuth';
  static const String selectInfo = 'selectInfo';
  static const String logout = 'logout';

  //Books functions
  static const String recommendedBooks = 'books/recommended';
  static const String getGenres = 'books/genres';
  static const String addReview = 'addReview';

  //Users functions
  static const String users = 'users';

  //Library functions
  static const String addBookLibrary = 'addBookLibrary';
  static const String getLibraryBooks = 'getLibraryBooks';
}
