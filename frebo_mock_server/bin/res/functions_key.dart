class AuthKeys {
  static const String token = 'token';
  static const String refreshToken = 'refresh_token';
  static const String user = 'user';
}

class LocationKeys {
  static const String value = 'value';
  static const String mode = 'mode';
}

class BooksKeys {
  static const String bookId = 'book_id';
  static const String review = 'review';
}
