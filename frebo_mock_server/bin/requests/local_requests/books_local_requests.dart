import 'dart:convert';
import 'dart:io';

import 'package:uuid/uuid.dart';

import '../../models/book.dart';
import '../../models/review.dart';
import '../../res/dummy_data.dart';

class BooksLocalStoreRequest {
  static void saveBooks() async {
    var title = 'books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      await file.writeAsString(jsonEncode(dummy_books.map((e) => e.toJson()).toList()));
    }
  }

  static Future<List<Book>> getBooks() async {
    var title = 'books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final books = <Book>[];
      jsonDecode(body).forEach((element) {
        books.add(Book.fromJson(element));
      });
      return books;
    }
  }

  //static Future<bool> containReviewBooks({String bookId, String userId}) async {
  //  var title = 'books';
//
  //  final file = await _getFile(title);
  //  final body = await file.readAsString();
  //  if (body == null || body.isEmpty) {
  //    return false;
  //  } else {
  //    final books = <Book>[];
  //    jsonDecode(body).forEach((element) {
  //      books.add(Book.fromJson(element));
  //    });
  //    final book = books.firstWhere((element) => element.bookId == bookId);
  //    return book.friends.indexWhere((element) => element.id == userId) != -1;
  //  }
  //}

  static Future<void> addReviewBooks({String bookId, Review review}) async {
    var title = 'books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final books = <Book>[];
      jsonDecode(body).forEach((element) {
        books.add(Book.fromJson(element));
      });
      final book = books.firstWhere((element) => element.id == bookId);
      review.id = Uuid().v4().substring(0, 8);
      book.reviews.add(review);
      book.ratCount++;
      var sum = 0.0;
      book.reviews.forEach((element) {
        sum += element.rating;
      });
      sum = sum / book.ratCount;
      book.rating = sum;
      books.removeWhere((element) => element.id == bookId);
      books.insert(0, book);
      await file.writeAsString(jsonEncode(books.map((e) => e.toJson()).toList()));
    }
  }

  static Future<File> _getFile(title) async {
    File file;
    if (!await File('../$title.txt').exists()) {
      file = await File('../$title.txt').create();
      print('Create file ' + file.path);
    } else {
      file = await File('../$title.txt');
    }
    return file;
  }
}
