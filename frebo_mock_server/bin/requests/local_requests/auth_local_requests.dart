import 'dart:convert';
import 'dart:io';

import 'package:uuid/uuid.dart';

import '../../models/token.dart';
import '../../models/user.dart';

class AuthLocalStoreRequest {
  static void saveNewToken(Token token) async {
    var title = 'tokens';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      await file.writeAsString(jsonEncode([token.toJson()]));
    } else {
      final tokens = [];
      jsonDecode(body).forEach((element) {
        tokens.add(Token.fromJson(element));
      });
      tokens.add(token);
      await file.writeAsString(jsonEncode(tokens.map((e) => e.toJson()).toList()));
    }
  }

  static void updateTokenUser(User user) async {
    var title = 'tokens';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final tokens = <Token>[];
    jsonDecode(body).forEach((element) {
      tokens.add(Token.fromJson(element));
    });
    tokens.forEach((element) => element.user = user);
    await file.writeAsString(jsonEncode(tokens.map((e) => e.toJson()).toList()));
  }

  static void removeTokenUser(Token token) async {
    var title = 'tokens';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final tokens = <Token>[];
    jsonDecode(body).forEach((element) {
      tokens.add(Token.fromJson(element));
    });
    tokens.removeWhere((element) => element.token == token.token);
    await file.writeAsString(jsonEncode(tokens.map((e) => e.toJson()).toList()));
  }

  static void updateUser(User user) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final users = <User>[];
    jsonDecode(body).forEach((element) {
      users.add(User.fromJson(element));
    });
    users.removeWhere((element) => element.email == user.email);
    users.add(user);
    await file.writeAsString(jsonEncode(users.map((e) => e.toJson()).toList()));
  }

  static void deleteUser(User user) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final users = <User>[];
    jsonDecode(body).forEach((element) {
      users.add(User.fromJson(element));
    });
    users.removeWhere((element) => element.email == user.email);
    await file.writeAsString(jsonEncode(users.map((e) => e.toJson()).toList()));
  }

  static Future<User> saveNewUser(User user) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      user.id = Uuid().v4().substring(0, 8);
      await file.writeAsString(jsonEncode([user.toJson()]));
      return user;
    } else {
      final users = <User>[];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      user.id = Uuid().v4().substring(0, 8);
      users.add(user);
      await file.writeAsString(jsonEncode(users.map((e) => e.toJson()).toList()));
      return user;
    }
  }

  static Future<bool> isContainUserEmail(String email) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      return false;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.indexWhere((element) => element.email == email) != -1;
    }
  }

  static Future<User> getUser(String email) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.firstWhere((element) => element.email == email, orElse: () => null);
    }
  }

  static Future<Token> getToken(String token) async {
    var title = 'tokens';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final tokens = <Token>[];
      jsonDecode(body).forEach((element) {
        tokens.add(Token.fromJson(element));
      });
      return tokens.firstWhere((element) => element.token == token, orElse: () => null);
    }
  }

  static Future<User> getUserByEmail(String email) async {
    var title = 'users';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final users = [];
      jsonDecode(body).forEach((element) {
        users.add(User.fromJson(element));
      });
      return users.firstWhere((element) => element.email == email, orElse: () => null);
    }
  }

  static Future<File> _getFile(title) async {
    File file;
    if (!await File('../$title.txt').exists()) {
      file = await File('../$title.txt').create();
      print('Create file ' + file.path);
    } else {
      file = await File('../$title.txt');
    }
    return file;
  }
}
