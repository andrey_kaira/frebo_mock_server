// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    username: json['username'] as String,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    imageUrl: json['imageUrl'] as String,
    phone: json['phone'] as String,
    email: json['email'] as String,
    address: json['address'] as String,
    gender: json['gender'] as String,
    birthday: json['birthday'] == null
        ? null
        : DateTime.parse(json['birthday'] as String),
    library: json['library'] == null
        ? null
        : Library.fromJson(json['library'] as Map<String, dynamic>),
    userStatus: json['userStatus'] as int,
    locationLat: json['locationLat'] as int,
    locationLng: json['locationLng'] as int,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'imageUrl': instance.imageUrl,
      'phone': instance.phone,
      'email': instance.email,
      'address': instance.address,
      'gender': instance.gender,
      'birthday': instance.birthday?.toIso8601String(),
      'userStatus': instance.userStatus,
      'locationLat': instance.locationLat,
      'locationLng': instance.locationLng,
      'library': instance.library,
    };
