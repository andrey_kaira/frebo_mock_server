import 'package:json_annotation/json_annotation.dart';
import '../res/functions_key.dart';
import 'user.dart';

part 'token.g.dart';

@JsonSerializable()
class Token {
  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  Map<String, dynamic> toJson() => _$TokenToJson(this);

  User user;
  String token;

  Token({
    this.user,
    this.token,
  });

  String toJsonResponse() {
    return token;
  }
}
