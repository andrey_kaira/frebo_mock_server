import 'package:json_annotation/json_annotation.dart';

part 'genre.g.dart';

@JsonSerializable()
class Genre {

	factory Genre.fromJson(Map<String, dynamic> json) => _$GenreFromJson(json);
	Map<String, dynamic> toJson() => _$GenreToJson(this);
  final String id;
  final String name;
  final String locale;
  final String desc;

  Genre({
    this.id,
    this.name,
    this.locale = 'en',
    this.desc,
  });
}
