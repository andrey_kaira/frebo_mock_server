import 'package:json_annotation/json_annotation.dart';

import 'book.dart';
import 'friend.dart';
import 'pickup_detalils.dart';

part 'book_request.g.dart';

@JsonSerializable()
class BookRequest {

	factory BookRequest.fromJson(Map<String, dynamic> json) => _$BookRequestFromJson(json);
	Map<String, dynamic> toJson() => _$BookRequestToJson(this);
  final int id;
  final Book book;
  final Friend friend;
  final String status;
  final String fromTo;
  final PickupDetails pickupDetails;

  BookRequest({
    this.id,
    this.book,
    this.friend,
    this.status,
    this.fromTo,
    this.pickupDetails,
  });
}

class FromTo {
  static const String FromMe = 'FROM_ME';
  static const String ToMe = 'TO_ME';
}
