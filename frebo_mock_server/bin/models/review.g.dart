// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Review _$ReviewFromJson(Map<String, dynamic> json) {
  return Review(
    id: json['id'] as String,
    rating: (json['rating'] as num)?.toDouble(),
    content: json['content'] as String,
    date: json['date'] == null ? null : DateTime.parse(json['date'] as String),
    friend: json['friend'] == null
        ? null
        : Friend.fromJson(json['friend'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'id': instance.id,
      'rating': instance.rating,
      'content': instance.content,
      'date': instance.date?.toIso8601String(),
      'friend': instance.friend,
    };
