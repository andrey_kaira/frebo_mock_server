// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Friend _$FriendFromJson(Map<String, dynamic> json) {
  return Friend(
    id: json['id'] as String,
    name: json['name'] as String,
    distance: json['distance'] as int,
    image: json['image'] as String,
    status: json['status'] as String,
    locationLat: json['locationLat'] as int,
    locationLng: json['locationLng'] as int,
  );
}

Map<String, dynamic> _$FriendToJson(Friend instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'distance': instance.distance,
      'locationLat': instance.locationLat,
      'locationLng': instance.locationLng,
      'image': instance.image,
      'status': instance.status,
    };
