import 'package:json_annotation/json_annotation.dart';
part 'registartion_model.g.dart';

@JsonSerializable()
class RegistrationModel {

	factory RegistrationModel.fromJson(Map<String, dynamic> json) => _$RegistrationModelFromJson(json);
	Map<String, dynamic> toJson( instance) => _$RegistrationModelToJson(this);
  final String gender;
  final String address;
  final DateTime birthday;
  final List<int> books;
  final List<double> cords;

  RegistrationModel({
    this.gender,
    this.address,
    this.birthday,
    this.books,
    this.cords,
  });
}
