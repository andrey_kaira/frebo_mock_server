import 'package:json_annotation/json_annotation.dart';
import 'friend.dart';
import 'genre.dart';
import 'review.dart';

part 'book.g.dart';

@JsonSerializable()
class Book {
  final String bookId;
  final String name;
  final String author;
  int distance;
  double rating;
  int ratingCount;
  final String summary;
  final String coverImageUrl;
  final List<Genre> genres;
  int friendsCount;


  Book({
    this.bookId,
    this.name,
    this.author,
    this.distance,
    this.rating,
    this.ratingCount,
    this.summary,
    this.coverImageUrl,
    this.genres,
    this.friendsCount,
  });

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);
  Map<String, dynamic> toJson() => _$BookToJson(this);
}