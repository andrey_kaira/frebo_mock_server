import 'package:json_annotation/json_annotation.dart';

part 'friend.g.dart';
@JsonSerializable()
class Friend {
	factory Friend.fromJson(Map<String, dynamic> json) => _$FriendFromJson(json);
	Map<String, dynamic> toJson() => _$FriendToJson(this);

  final String id;
  final String name;
  final int distance;
  final int locationLat;
  final int locationLng;
  final String image;
  final String status;

  Friend({
    this.id,
    this.name,
    this.distance,
    this.image,
    this.status,
    this.locationLat,
    this.locationLng,
  });
}
