// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookRequest _$BookRequestFromJson(Map<String, dynamic> json) {
  return BookRequest(
    id: json['id'] as int,
    book: json['book'] == null
        ? null
        : Book.fromJson(json['book'] as Map<String, dynamic>),
    friend: json['friend'] == null
        ? null
        : Friend.fromJson(json['friend'] as Map<String, dynamic>),
    status: json['status'] as String,
    fromTo: json['fromTo'] as String,
    pickupDetails: json['pickupDetails'] == null
        ? null
        : PickupDetails.fromJson(json['pickupDetails'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$BookRequestToJson(BookRequest instance) =>
    <String, dynamic>{
      'id': instance.id,
      'book': instance.book,
      'friend': instance.friend,
      'status': instance.status,
      'fromTo': instance.fromTo,
      'pickupDetails': instance.pickupDetails,
    };
