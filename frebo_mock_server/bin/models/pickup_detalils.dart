import 'package:json_annotation/json_annotation.dart';

part 'pickup_detalils.g.dart';
@JsonSerializable()
class PickupDetails {
	factory PickupDetails.fromJson(Map<String, dynamic> json) => _$PickupDetailsFromJson(json);
	Map<String, dynamic> toJson() => _$PickupDetailsToJson(this);

  final String address;
  final String place;
  final String day;
  final DateTime time;

  PickupDetails({
    this.address,
    this.place,
    this.day,
    this.time,
  });
}
