import 'package:json_annotation/json_annotation.dart';
import 'library.dart';
import 'registartion_model.dart';
import 'token.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  String id;
  final String username;
  final String firstName;
  final String lastName;
  final String imageUrl;
  final String phone;
  final String email;
  final String address;
  final String gender;
  final DateTime birthday;
  final int userStatus;
  final int locationLat;
  final int locationLng;
  Library library;

  User({
    this.id,
    this.username,
    this.firstName,
    this.lastName,
    this.imageUrl,
    this.phone,
    this.email,
    this.address,
    this.gender,
    this.birthday,
    this.library,
    this.userStatus = 0,
    this.locationLat = 0,
    this.locationLng = 0,
  });

  Map<String, dynamic> toJsonResponse(Token token) {
    return toJson()..addEntries([MapEntry('token', token.toJsonResponse())]);
  }

  factory User.insertData({User user, RegistrationModel registrationModel}) {
    return User(
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      birthday: registrationModel.birthday,
      address: registrationModel.address,
      gender: registrationModel.gender,
      imageUrl: user.imageUrl,
      username: user.username,
      userStatus: 0,
      locationLat: 0,
      locationLng: 0,
    );
  }
}
