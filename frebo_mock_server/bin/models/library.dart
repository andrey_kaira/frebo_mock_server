import 'package:json_annotation/json_annotation.dart';

import 'book.dart';

part 'library.g.dart';

@JsonSerializable()
class Library {

	factory Library.fromJson(Map<String, dynamic> json) => _$LibraryFromJson(json);
	Map<String, dynamic> toJson() => _$LibraryToJson(this);

  final List<Book> books;

  Library({
    this.books,
  });
}
