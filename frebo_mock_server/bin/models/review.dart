import 'package:json_annotation/json_annotation.dart';
import 'friend.dart';

part 'review.g.dart';

@JsonSerializable()
class Review {

	factory Review.fromJson(Map<String, dynamic> json) => _$ReviewFromJson(json);
	Map<String, dynamic> toJson() => _$ReviewToJson(this);

	String id;
  final double rating;
  final String content;
  final DateTime date;
  Friend friend;

  Review({
    this.id,
    this.rating,
    this.content,
    this.date,
    this.friend,
  });
}
