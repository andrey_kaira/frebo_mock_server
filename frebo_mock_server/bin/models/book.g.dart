// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Book _$BookFromJson(Map<String, dynamic> json) {
  return Book(
    bookId: json['bookId'] as String,
    name: json['name'] as String,
    author: json['author'] as String,
    distance: json['distance'] as int,
    rating: (json['rating'] as num)?.toDouble(),
    ratingCount: json['ratingCount'] as int,
    summary: json['summary'] as String,
    coverImageUrl: json['coverImageUrl'] as String,
    genres: (json['genres'] as List)
        ?.map(
            (e) => e == null ? null : Genre.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    friendsCount: json['friendsCount'] as int,
  );
}

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'bookId': instance.bookId,
      'name': instance.name,
      'author': instance.author,
      'distance': instance.distance,
      'rating': instance.rating,
      'ratingCount': instance.ratingCount,
      'summary': instance.summary,
      'coverImageUrl': instance.coverImageUrl,
      'genres': instance.genres,
      'friendsCount': instance.friendsCount,
    };
