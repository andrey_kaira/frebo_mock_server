// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registartion_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegistrationModel _$RegistrationModelFromJson(Map<String, dynamic> json) {
  return RegistrationModel(
    gender: json['gender'] as String,
    address: json['address'] as String,
    birthday: json['birthday'] == null
        ? null
        : DateTime.parse(json['birthday'] as String),
    books: (json['books'] as List)?.map((e) => e as int)?.toList(),
    cords:
        (json['cords'] as List)?.map((e) => (e as num)?.toDouble())?.toList(),
  );
}

Map<String, dynamic> _$RegistrationModelToJson(RegistrationModel instance) =>
    <String, dynamic>{
      'gender': instance.gender,
      'address': instance.address,
      'birthday': instance.birthday?.toIso8601String(),
      'books': instance.books,
      'cords': instance.cords,
    };
