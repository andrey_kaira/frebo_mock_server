// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pickup_detalils.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PickupDetails _$PickupDetailsFromJson(Map<String, dynamic> json) {
  return PickupDetails(
    address: json['address'] as String,
    place: json['place'] as String,
    day: json['day'] as String,
    time: json['time'] == null ? null : DateTime.parse(json['time'] as String),
  );
}

Map<String, dynamic> _$PickupDetailsToJson(PickupDetails instance) =>
    <String, dynamic>{
      'address': instance.address,
      'place': instance.place,
      'day': instance.day,
      'time': instance.time?.toIso8601String(),
    };
