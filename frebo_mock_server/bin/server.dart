import 'dart:io';
import 'dart:convert';

import 'package:args/args.dart';
import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;

import 'functions/books/books_manager.dart';
import 'functions/library/library_manager.dart';
import 'functions/users/users_manager.dart';
import 'requests/local_requests/books_local_requests.dart';
import 'res/api_key.dart';
import 'functions/auth/auth_manager.dart';
import 'functions/shared/not_found_functions.dart';
import 'sockets/location_socket.dart';
import 'sockets/serach_books_socket.dart';

const _hostname = 'localhost';
//const _hostname = '165.232.155.214';

void main(List<String> args) async {
  var parser = ArgParser()..addOption('port', abbr: 'p');
  var result = parser.parse(args);

  var portStr = result['port'] ?? Platform.environment['PORT'] ?? '8080';
  var port = int.tryParse(portStr);

  if (port == null) {
    stdout.writeln('Could not parse port value "$portStr" into a number.');
    exitCode = 64;
    return;
  }

  var handler = const shelf.Pipeline().addMiddleware(shelf.logRequests()).addHandler(_echoRequest);

  var server = await io.serve(handler, _hostname, port);
  print('Serving at http://${server.address.host}:${server.port}');

  await LocationSocket.init(ip: server.address.host, port: 4567);
  await SearchBooksSocket.init(ip: server.address.host, port: 4568);
  BooksLocalStoreRequest.saveBooks();
}

Future<shelf.Response> _echoRequest(shelf.Request request) async {
  final body = _getBody(await request.readAsString()) ?? {};
  final path = request.url.path;
  final headers = request.headers;
  final method = request.method;
  switch (request.url.path) {
    case FunctionsKeys.facebookAuth:
      return await AuthManager.authFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.selectInfo:
      return await AuthManager.authFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.logout:
      return await AuthManager.authFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.recommendedBooks:
      return await BooksManager.booksFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.addReview:
      return await BooksManager.booksFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.getGenres:
      return await BooksManager.booksFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.addBookLibrary:
      return await LibraryManager.libraryFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.getLibraryBooks:
      return await LibraryManager.libraryFunctionsManager(path, headers: headers, body: body);
    case FunctionsKeys.users:
      return await UsersManager.usersFunctionsManager(path, method: method, headers: headers, body: body);
    default:
      return NotFoundFunctions.notFoundFunction();
  }
}

Map<String, dynamic> _getBody(String body) {
  try {
    if (body == null || body == '') return null;
    var data = <MapEntry<String, dynamic>>[];
    var index = 1;
    const kSearchKey = 'Content-Disposition: form-data; name=';
    if (body.contains(kSearchKey)) {
      var encodeStr = jsonEncode(body);
      var clearStr = encodeStr.replaceAll('\\r', '').replaceAll('\\n', '');
      while (index != kSearchKey.length + 1) {
        index = clearStr.indexOf(kSearchKey, index) + kSearchKey.length + 2;
        if (index != kSearchKey.length + 1) {
          final key = clearStr.substring(index, clearStr.indexOf('\\"', index));
          final value = clearStr.substring(index + key.length + 2, clearStr.indexOf('------------------------', index)).replaceAll('\\"', '"');
          data.add(MapEntry(key, value));
        }
      }
    } else if (body.substring(0, 1) == '{') {
      return jsonDecode(body);
    } else {
      body = body.replaceAll('=', '" : "').replaceAll('&', '", "');
      body = '{"$body"}';
      return jsonDecode(body);
    }
    return Map.fromEntries(data);
  } catch (error) {
    print('Read body error');
    return null;
  }
}
