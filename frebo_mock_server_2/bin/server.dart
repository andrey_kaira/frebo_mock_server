import 'dart:convert';
import 'dart:io';

import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;

import 'functions/auth/auth_manager.dart';
import 'functions/books/books_manager.dart';
import 'functions/library/library_manager.dart';
import 'functions/requests/requests_manager.dart';
import 'functions/shared/not_found_functions.dart';
import 'functions/users/users_manager.dart';
import 'requests/local_requests/books_local_requests.dart';
import 'res/api_keys.dart';
import 'socket/chat_socket.dart';

const _port = 8080;

void main(List<String> args) async {
  var hostname = (await NetworkInterface.list()).first.addresses.first.address;

  var handler = const shelf.Pipeline().addMiddleware(shelf.logRequests()).addHandler(_echoRequest);

  var server = await io.serve(handler, hostname, _port);
  print('Serving at http://${server.address.host}:${server.port}');

  await ChatSocket.init(ip: server.address.host, port: 4568);

  BooksLocalStoreRequest.saveBooks();
}

Future<shelf.Response> _echoRequest(shelf.Request request) async {
  final body = _getBody((await request.readAsString()) ?? {});
  final pathSegments = request.url.pathSegments;
  final headers = request.headers;
  final method = request.method;
  final queryParameters = request.url.queryParameters;
  if (pathSegments.isEmpty) {
    return NotFoundFunctions.notFoundFunction();
  }
  switch (pathSegments.first) {
    case ApiKeys.auth:
      return AuthManager.functionsManager(pathSegments, headers: headers, body: body);
    case ApiKeys.users:
      return UsersManager.functionsManager(pathSegments, headers: headers, body: body, method: method);
    case ApiKeys.books:
      return BooksManager.functionsManager(pathSegments, headers: headers, body: body, method: method, queryParameters: queryParameters);
    case ApiKeys.libraries:
      return LibraryManager.functionsManager(pathSegments, headers: headers, body: body, method: method, queryParameters: queryParameters);
   case ApiKeys.requests:
      return RequestsManager.functionsManager(pathSegments, headers: headers, body: body, method: method, queryParameters: queryParameters);
    default:
      return NotFoundFunctions.notFoundFunction();
  }
}

dynamic _getBody(String body) {
  try {
    return jsonDecode(body);
  } catch (error) {
    print('Read body error');
    return null;
  }
}
