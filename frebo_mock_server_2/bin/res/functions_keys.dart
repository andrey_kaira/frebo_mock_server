class AuthKeys {
  static const String token = 'Authorization';
  static const String firebaseToken = 'firebaseToken';
  static const String user = 'user';
}

class LocationKeys {
  static const String value = 'value';
  static const String mode = 'mode';
}

class BooksKeys {
  static const String bookId = 'bookId';
  static const String review = 'review';
  static const String count = 'count';
  static const String offset = 'offset';
  static const String distanceFromLocation = 'distanceFromLocation';
  static const String lat = 'lat';
  static const String lng  = 'lng';
  static const String q  = 'q';
  static const String barcode  = 'barcode';
  static const String id  = 'id';
  static const String rating  = 'rating';
}

class RequestKeys {
  static const String requestId  = 'requestId';
}
