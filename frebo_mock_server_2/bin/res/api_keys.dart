class ApiKeys {
  static const String auth = 'auth';
  static const String login = 'login';
  static const String logout = 'logout';

  static const String users = 'users';

  static const String books = 'books';
  static const String forRegistration = 'forRegistration';
  static const String genres = 'genres';
  static const String recommended = 'recommended';
  static const String search = 'search';
  static const String searchForLibrary = 'searchForLibrary';
  static const String searchBarCode = 'searchBarCode';
  static const String reviews = 'reviews';
  static const String ratings = 'ratings';
  static const String friends = 'friends';

  static const String libraries = 'libraries';

  static const String requests = 'requests';
  static const String cancelRequest = 'cancelRequest';
  static const String accept = 'accept';
  static const String bookTakenOrPicked = 'bookTakenOrPicked';
  static const String bookReturned = 'bookReturned';
}
