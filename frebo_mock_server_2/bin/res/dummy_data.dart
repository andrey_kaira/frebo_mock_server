import 'package:uuid/uuid.dart';

import '../models/book.dart';
import '../models/genre.dart';

List<Genre> dummy_genre = [
  Genre(id: Uuid().v4().substring(0, 8), name: 'Action and Adventure', desc: 'Action and adventure books constantly have you on the edge of your seat with excitement, as your fave main character repeatedly finds themselves in high stakes situations. The protagonist has an ultimate goal to achieve and is always put in risky, often dangerous situations. This genre typically crosses over with others like mystery, crime, sci-fi, and fantasy.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Classics', desc: 'You may think of these books as the throwback readings you were assigned in English class. (Looking at you, Charles Dickens.) The classics have been around for decades, and were often groundbreaking stories at their publish time, but have continued to be impactful for generations, serving as the foundation for many popular works we read today.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Comic Book or Graphic Novel', desc: 'The stories in comic books and graphic novels are presented to the reader through engaging, sequential narrative art (illustrations and typography) that\'s either presented in a specific design or the traditional panel layout you find in comics. With both, you\'ll often find the dialogue presented in the tell-tale "word balloons" next to the respective characters.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Detective and Mystery', desc: 'The plot always revolves around a crime of sorts that must be solved—or foiled—by the protagonists.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Fantasy', desc: 'While usually set in a fictional imagined world—in opposition, Ta-Nehisi\'s Coates\'s The Water Dancer takes place in the very real world of American slavery—fantasy books include prominent elements of magic, mythology, or the supernatural.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Historical Fiction', desc: 'These books are based in a time period set in the past decades, often against the backdrop of significant (real) historical events.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Horror', desc: 'Meant to cause discomfort and fear for both the character and readers, horror writers often make use of supernatural and paranormal elements in morbid stories that are sometimes a little too realistic. The master of horror fiction? None other than Stephen King.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Literary Fiction', desc: 'Though it can be seen as a broad genre that encompasses many others, literary fiction refers to the perceived artistic writing style of the author. Their prose is meant to evoke deep thought through stories that offer personal or social commentary on a particular theme.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Romance', desc: 'Oh romance, how could we ever resist you? The genre that makes your heart all warm and fuzzy focuses on the love story of the main protagonists. This world of fiction is extremely wide-reaching in and of itself, as it has a variety of sub-genres including: contemporary romance, historical, paranormal, and the steamier erotica. If you\'re in need of any suggestions, we\'ve got a list of the best romances of all time and the top picks of the year.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Science Fiction (Sci-Fi)', desc: 'Though they\'re often thought of in the same vein as fantasy, what distinguishes science fiction stories is that they lean heavily on themes of technology and future science. You\'ll find apocalyptic and dystopian novels in the sci-fi genre as well.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Short Stories', desc: 'Though they encompass many of the genres we describe here, short stories are brief prose that are significantly, well, shorter than novels. Writers strictly tell their narratives through a specific theme and a series of brief scenes, though many authors compile these stories in wide-ranging collections, as featured below.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Suspense and Thrillers', desc: 'While they often encompass the same elements as mystery books, the suspense and thriller genre sees the hero attempt to stop and defeat the villain to save their own life rather than uncover a specific crime. Thrillers typically include cliffhangers and deception to encourage suspense, while pulling the wool over the eyes of both the main character and reader.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Women\'s Fiction', desc: 'Another genre that encompasses many others, women\'s fiction is written specifically to target female readers, often reflecting on the shared experiences of being a woman in society and the protagonist\'s personal growth.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Biographies and Autobiographies', desc: 'Serving as an official account of the details and events of a person\'s life span, autobiographies are written by the subject themselves, while biographies are written by an author who is not the focus of the book.'),
  Genre(id: Uuid().v4().substring(0, 8), name: 'Cookbooks', desc: 'Serving as an official account of the details and events of a person\'s life span, autobiographies are written by the subject themselves, while biographies are written by an author who is not the focus of the book.'),
];

List<Book> dummy_books = <Book>[
  Book(
      bookId: Uuid().v4().substring(0, 8),
      name: 'The Body: A Guide for Occupants',
      summary: '#1 Bestseller in both hardback and paperback: SHORTLISTED FOR THE 2020 ROYAL SOCIETY INSIGHT INVESTMENT SCIENCE BOOK PRIZE',
      distance: 10,
      author: 'Bill Bryson',
      coverImageUrl: 'https://m.media-amazon.com/images/I/51LMZBNahWL.jpg',
      genres: [
        Genre(id: Uuid().v4().substring(0, 8), name: 'Adult Psychology'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Anatomy & Physiology'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Medical Anatomy'),
      ]),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Saint X: A Novel',
    summary:
        'A New York Times Notable Book of 2020\n\n\"\'Saint X\' is hypnotic. Schaitkin\'s characters...are so intelligent and distinctive it feels not just easy, but necessary, to follow them. I devoured [it] in a day."–Oyinkan Braithwaite, New York Times Book Review',
    distance: 15,
    author: 'Alexis Schaitkin',
    coverImageUrl: 'https://m.media-amazon.com/images/I/51pMrW0+vcL.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'City Life Fiction'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Urban Fiction'),
    ],
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Me: Elton John Official Autobiography',
    summary:
        'INSTANT #1 NEW YORK TIMES BESTSELLER\n\nIn his first and only official autobiography, music icon Elton John reveals the truth about his extraordinary life, from his rollercoaster lifestyle as shown in the film Rocketman, to becoming a living legend.',
    distance: 20,
    author: 'Elton John',
    coverImageUrl: 'https://m.media-amazon.com/images/I/51y8+fZ4bcL.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Pop Artist Biographies'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Popular Music (Books)', desc: 'Desc!'),
    ],
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Wild Game: My Mother, Her Secret, and Me',
    summary:
        'Great Experience. Great Value.\nEnjoy a great reading experience when you buy the Kindle edition of this book. Learn more about Great on Kindle, available in select categories.',
    distance: 2,
    author: 'Adrienne Brodeur',
    coverImageUrl: 'https://m.media-amazon.com/images/I/413kTDJpUKL.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Motherhood'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Biographies of Women'),
    ],
  ),
  Book(
      bookId: Uuid().v4().substring(0, 8),
      name: 'Prosper\'s Demon',
      summary:
          '"As if Deadpool had slipped into the body of the Witcher Geralt." —The New York Times\n\nIn the pitch dark, witty fantasy novella Prosper\'s Demon, K. J. Parker deftly creates a world with vivid, unbending rules, seething with demons, broken faith, and worse men.',
      distance: 10,
      author: 'K. J. Parker',
      coverImageUrl: 'https://m.media-amazon.com/images/I/41b86Pk3JkL.jpg',
      genres: [
        Genre(id: Uuid().v4().substring(0, 8), name: 'Christian Thriller & Suspense'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Christian Mystery & Suspense', desc: 'Desc!'),
      ]),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Tucker: Eternity Springs: The McBrides of Texas',
    summary:
        'In Eternity Springs: The McBrides of Texas, New York Times bestselling author Emily March presents a brand new arc set in the Lone Star State that features a family-linked trilogy within the author\'s romantic series.',
    distance: 100,
    author: 'Emily March',
    coverImageUrl: 'https://m.media-amazon.com/images/I/51yxyi4FQ7L.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Contemporary Romance'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Military Romance'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Contemporary Romance', desc: 'Desc!'),
    ],
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Forever My Duke: Unlikely Duchesses',
    summary:
        'Forever My Duke is the second novel in a brand new Regency romance series from Olivia Drake about rakish dukes and the governesses who steal their hearts.“I find Miss Fanshawe to be quite charming—for an American.”—The Prince Regent',
    distance: 25,
    author: 'Olivia Drake',
    coverImageUrl: 'https://m.media-amazon.com/images/I/51200dkZwOL.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Regency Romance'),
    ],
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'How to Catch a Leprechaun',
    summary:
        'The #1 New York Times and USA Today bestseller perfect for St. Patrick\'s Day and beyond!\n\nYou\'ve been planning night and day, and finally you\'ve created the perfect trap with shamrocks, pots of gold, and rainbows galore! Now all you need to do is wait. Is this the year you\'ll finally catch the leprechaun? Start a St. Patrick\'s Day tradition with this fun and lively children\'s picture book and get inspired to build leprechaun traps of your own at home or in the classroom! Laugh along in this zany story for kids that blends STEAM concepts with hilarious rhymes and vibrant illustrations!',
    distance: 90,
    author: 'Adam Wallace',
    coverImageUrl: 'https://images-na.ssl-images-amazon.com/images/I/61DY-xNKbGL._SY498_BO1,204,203,200_.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'Poetry & Nursery Rhymes for Children'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Holidays & Celebrations for Children'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Non-religious Holiday Books'),
    ],
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Earth: By The Numbers',
    summary:
        'Caldecott Honor winner Steve Jenkins introduces By the Numbers infographic readers chock full of incredible infographs and stunning, full-color cut-paper illustrations. Earth will focus on the fascinating ins-and-outs of earth science.',
    distance: 20,
    author: 'Steve Jenkins',
    coverImageUrl: 'https://m.media-amazon.com/images/I/41Ay5Pxt2cL.jpg',
    genres: [
      Genre(id: Uuid().v4().substring(0, 8), name: 'City Life Fiction'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Holidays & Celebrations for Children'),
      Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Non-religious Holiday Books'),
    ],
  ),
  Book(
      bookId: Uuid().v4().substring(0, 8),
      name: 'If Animals Kissed Good Night',
      summary:
          'Don\'t miss the other books in this adorable series: If Animals Said I Love You, If Animals Celebrated Christmas, If Animals Went to School, and If Animals Gave Thanks!',
      distance: 150,
      author: 'Ann Whitford Paul ',
      coverImageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51PRQuO-xjL._SY498_BO1,204,203,200_.jpg',
      genres: [
        Genre(id: Uuid().v4().substring(0, 8), name: 'Motherhood'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Baby Animal Books'),
        Genre(id: Uuid().v4().substring(0, 8), name: 'Children\'s Parents Books'),
      ]),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Baby Sign Language Made Easy: 101 Signs to Start Communicating with Your Child Now',
    summary: 'The easy way to teach sign language to babies and toddlers ages 0 to 3',
    distance: 2,
    author: 'Lane Rebelo',
    coverImageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51WHRpkDS4L._SX474_BO1,204,203,200_.jpg',
    genres: [],
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name:
        'The Unofficial Disney Parks Cookbook: From Delicious Dole Whip to Tasty Mickey Pretzels, 100 Magical Disney-Inspired Recipes (Unofficial Cookbook) ',
    summary:
        'Great Experience. Great Value.\nEnjoy a great reading experience when you buy the Kindle edition of this book. Learn more about Great on Kindle, available in select categories.',
    distance: 10,
    author: 'Ashley Craft',
    coverImageUrl: 'https://images-na.ssl-images-amazon.com/images/I/51QBFxYP2tL._SX382_BO1,204,203,200_.jpg',
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'Life After Death: A Novel',
    summary: 'The long-anticipated sequel to Sister Souljah’s million copy bestseller The Coldest Winter Ever.',
    distance: 10,
    author: 'Sister Souljah ',
    coverImageUrl: 'https://images-na.ssl-images-amazon.com/images/I/41FYi16jzwL._SX327_BO1,204,203,200_.jpg',
  ),
  Book(
    bookId: Uuid().v4().substring(0, 8),
    name: 'A Promised Land',
    summary: 'A riveting, deeply personal account of history in the making—from the president who inspired us to believe in the power of democracy',
    distance: 200,
    author: 'Barack Obama',
    coverImageUrl: 'https://sun9-30.userapi.com/c844616/v844616011/f02dc/40k1XZ-4M8g.jpg',
  ),
];
