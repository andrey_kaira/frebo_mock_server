import 'package:json_annotation/json_annotation.dart';

import 'user.dart';

part 'library.g.dart';

@JsonSerializable()
class Library {
  factory Library.fromJson(Map<String, dynamic> json) => _$LibraryFromJson(json);

  Map<String, dynamic> toJson() => _$LibraryToJson(this);
  final String id;
  final String name;
  bool isUserLibrary;
  User user;
  final String status;

  Library({
    this.id,
    this.name,
    this.isUserLibrary,
    this.user,
    this.status,
  });

  Map<String, dynamic> toJsonLibrary() {
    return {
      'id': id,
      'name': name,
      'isUserLibrary': isUserLibrary,
      'user': user.toJsonLibrary(),
    };
  }

  Map<String, dynamic> toJsonBooks() {
    return {
      'libraryId': id,
      'status': status,
    };
  }
}
