// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Book _$BookFromJson(Map<String, dynamic> json) {
  return Book(
    bookId: json['bookId'] as String,
    name: json['name'] as String,
    coverImageUrl: json['coverImageUrl'] as String,
    author: json['author'] as String,
    distance: json['distance'] as int,
    rating: json['rating'] as int,
    ratingCount: json['ratingCount'] as int,
    myRating: json['myRating'] as int,
    friendsCount: json['friendsCount'] as int,
    summary: json['summary'] as String,
    genres: (json['genres'] as List)
        ?.map(
            (e) => e == null ? null : Genre.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'bookId': instance.bookId,
      'name': instance.name,
      'coverImageUrl': instance.coverImageUrl,
      'author': instance.author,
      'distance': instance.distance,
      'rating': instance.rating,
      'ratingCount': instance.ratingCount,
      'myRating': instance.myRating,
      'friendsCount': instance.friendsCount,
      'summary': instance.summary,
      'genres': instance.genres,
    };
