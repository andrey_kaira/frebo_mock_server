import 'package:json_annotation/json_annotation.dart';
import 'user.dart';

part 'friend.g.dart';

@JsonSerializable()
class Friend {

	factory Friend.fromJson(Map<String, dynamic> json) => _$FriendFromJson(json);
	Map<String, dynamic> toJson() => _$FriendToJson(this);
  final String activeRequestStatus;
  final String libraryBookId;
  final String requestId;
  final User user;

  Friend({
    this.activeRequestStatus,
    this.libraryBookId,
    this.requestId,
    this.user,
  });

  Map<String, dynamic> toJsonRequest() {
    return {
      'activeRequestStatus': activeRequestStatus,
      'libraryBookId': libraryBookId,
      'requestId': requestId,
      'user': user.toJsonLibrary(),
    };
  }
}
