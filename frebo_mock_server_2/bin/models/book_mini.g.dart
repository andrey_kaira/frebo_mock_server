// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_mini.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookMini _$BookMiniFromJson(Map<String, dynamic> json) {
  return BookMini(
    bookId: json['bookId'] as String,
    coverImageUrl: json['coverImageUrl'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$BookMiniToJson(BookMini instance) => <String, dynamic>{
      'bookId': instance.bookId,
      'coverImageUrl': instance.coverImageUrl,
      'name': instance.name,
    };
