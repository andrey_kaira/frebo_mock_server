// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestDto _$RequestDtoFromJson(Map<String, dynamic> json) {
  return RequestDto(
    requestId: json['requestId'] as String,
    libraryBookId: json['libraryBookId'] as String,
    requestDate: json['requestDate'] == null
        ? null
        : DateTime.parse(json['requestDate'] as String),
    scheduledDelivery: json['scheduledDelivery'] == null
        ? null
        : DateTime.parse(json['scheduledDelivery'] as String),
    scheduledReturn: json['scheduledReturn'] == null
        ? null
        : DateTime.parse(json['scheduledReturn'] as String),
    status: json['status'] as String,
    sender: json['sender'] == null
        ? null
        : User.fromJson(json['sender'] as Map<String, dynamic>),
    recipient: json['recipient'] == null
        ? null
        : User.fromJson(json['recipient'] as Map<String, dynamic>),
    hasNewMessages: json['hasNewMessages'] as bool,
    book: json['book'] == null
        ? null
        : BookMini.fromJson(json['book'] as Map<String, dynamic>),
  )..distance = json['distance'] as int;
}

Map<String, dynamic> _$RequestDtoToJson(RequestDto instance) =>
    <String, dynamic>{
      'requestId': instance.requestId,
      'libraryBookId': instance.libraryBookId,
      'requestDate': instance.requestDate?.toIso8601String(),
      'scheduledDelivery': instance.scheduledDelivery?.toIso8601String(),
      'scheduledReturn': instance.scheduledReturn?.toIso8601String(),
      'status': instance.status,
      'sender': instance.sender,
      'recipient': instance.recipient,
      'hasNewMessages': instance.hasNewMessages,
      'distance': instance.distance,
      'book': instance.book,
    };
