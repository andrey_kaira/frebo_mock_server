// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as String,
    address: json['address'] as String,
    userStatus: json['userStatus'] as int,
    lastName: json['lastName'] as String,
    imageUrl: json['imageUrl'] as String,
    firstName: json['firstName'] as String,
    gender: json['gender'] as String,
    bookList: (json['bookList'] as List)
        ?.map((e) =>
            e == null ? null : BookIdDto.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    location: json['location'] == null
        ? null
        : LocationModel.fromJson(json['location'] as Map<String, dynamic>),
    isFirstLogin: json['isFirstLogin'] as bool,
    distance: json['distance'] as int,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'address': instance.address,
      'userStatus': instance.userStatus,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'imageUrl': instance.imageUrl,
      'gender': instance.gender,
      'bookList': instance.bookList,
      'location': instance.location,
      'distance': instance.distance,
      'isFirstLogin': instance.isFirstLogin,
    };
