import 'package:json_annotation/json_annotation.dart';

part 'add_library_books.g.dart';

@JsonSerializable()
class AddLibraryBooks {
  factory AddLibraryBooks.fromJson(Map<String, dynamic> json) => _$AddLibraryBooksFromJson(json);

  Map<String, dynamic> toJson(instance) => _$AddLibraryBooksToJson(this);
  final String libraryId;
  final String bookId;

  AddLibraryBooks({
    this.libraryId,
    this.bookId,
  });
}
