import 'package:json_annotation/json_annotation.dart';

part 'book_mini.g.dart';

@JsonSerializable()
class BookMini {

	factory BookMini.fromJson(Map<String, dynamic> json) => _$BookMiniFromJson(json);
	Map<String, dynamic> toJson() => _$BookMiniToJson(this);

  final String bookId;
  final String coverImageUrl;
  final String name;

  BookMini({
    this.bookId,
    this.coverImageUrl,
    this.name,
  });
}
