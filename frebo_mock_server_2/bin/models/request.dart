import 'package:json_annotation/json_annotation.dart';

import 'book_mini.dart';
import 'user.dart';

part 'request.g.dart';

@JsonSerializable()
class RequestDto {
  factory RequestDto.fromJson(Map<String, dynamic> json) => _$RequestDtoFromJson(json);

  Map<String, dynamic> toJson() => _$RequestDtoToJson(this);
  final String requestId;
  final String libraryBookId;
  DateTime requestDate;
  DateTime scheduledDelivery;
  DateTime scheduledReturn;
  String status;
  User sender;
  User recipient;
  bool hasNewMessages;
  int distance;
  final BookMini book;

  RequestDto({
    this.requestId,
    this.libraryBookId,
    this.requestDate,
    this.scheduledDelivery,
    this.scheduledReturn,
    this.status = RequestStatus.initialRequest,
    this.sender,
    this.recipient,
    this.hasNewMessages = true,
    this.book,
  }) {
    requestDate ??= DateTime.now();
  }

  Map<String, dynamic> toJsonRequest() {
    return {
      'requestId': requestId,
      'libraryBookId': libraryBookId,
      'requestDate': requestDate?.toString(),
      'scheduledDelivery': scheduledDelivery?.toString(),
      'scheduledReturn': scheduledReturn?.toString(),
      'status': status,
      'sender': sender.toJsonLibrary(),
      'recipient': recipient.toJsonLibrary(),
      'book': book,
      'hasNewMessages': hasNewMessages,
    };
  }

  @override
  String toString() {
    return 'RequestDto{requestId: $requestId, libraryBookId: $libraryBookId, requestDate: $requestDate, scheduledDelivery: $scheduledDelivery, scheduledReturn: $scheduledReturn, status: $status, sender: $sender, recipient: $recipient, book: $book}';
  }
}

class RequestStatus {
  static const initialRequest = 'Initial_Request';
  static const locationAndDateTimeOffered = 'Location_And_DateTime_Offered';
  static const awaitingDelivery = 'Awaiting_Delivery';
  static const bookTaken = 'Book_Taken';
  static const bookReturned = 'Book_Returned';
  static const cancelled = 'Cancelled';
}

