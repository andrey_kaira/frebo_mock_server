import 'package:json_annotation/json_annotation.dart';

part 'accept_request_model.g.dart';

@JsonSerializable()
class AcceptRequestModel {
  factory AcceptRequestModel.fromJson(Map<String, dynamic> json) => _$AcceptRequestModelFromJson(json);

  Map<String, dynamic> toJson(instance) => _$AcceptRequestModelToJson(this);
  final String requestId;
  final String pickupLocation;
  final String scheduledPickup;
  final String pickupTime;

  const AcceptRequestModel({
    this.requestId,
    this.pickupLocation,
    this.scheduledPickup,
    this.pickupTime,
  });
}
