import 'package:json_annotation/json_annotation.dart';

import 'genre.dart';

part 'book.g.dart';

@JsonSerializable()
class Book {
  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);

  Map<String, dynamic> toJson() => _$BookToJson(this);
  String bookId;
  String name;
  String coverImageUrl;
  String author;
  int distance;
  int rating;
  int ratingCount;
  int myRating;
  int friendsCount;
  String summary;
  List<Genre> genres;

  Book({
    this.bookId,
    this.name,
    this.coverImageUrl,
    this.author,
    this.distance,
    this.rating,
    this.ratingCount = 0,
    this.myRating = 0,
    this.friendsCount,
    this.summary,
    this.genres,
  });

  @override
  String toString() {
    return 'Book{bookId: $bookId, name: $name, coverImageUrl: $coverImageUrl, author: $author, distance: $distance, rating: $rating, ratingCount: $ratingCount, myRating: $myRating, friendsCount: $friendsCount, summary: $summary, genres: $genres}';
  }
}
