import 'package:json_annotation/json_annotation.dart';

import 'book_id_dto.dart';
import 'location_model.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  String id;
  String address;
  int userStatus;
  final String firstName;
  final String lastName;
  String imageUrl;
  String gender;
  List<BookIdDto> bookList;
  LocationModel location;
  int distance;
  bool isFirstLogin;

  User({
    this.id,
    this.address,
    this.userStatus,
    this.lastName,
    this.imageUrl,
    this.firstName,
    this.gender,
    this.bookList,
    this.location,
    this.isFirstLogin,
    this.distance = 0,
  });

  User copyWith({User user}) {
    return User(
      id: user.id ?? id,
      address: user.address ?? address,
      userStatus: user.userStatus ?? userStatus,
      firstName: user.firstName ?? firstName,
      lastName: user.lastName ?? lastName,
      imageUrl: user.imageUrl ?? imageUrl,
      gender: user.gender ?? gender,
      bookList: user.bookList ?? bookList,
      location: user.location ?? location,
      distance: user.distance ?? distance,
      isFirstLogin: user.isFirstLogin ?? isFirstLogin,
    );
  }

  Map<String, dynamic> toJsonAuth() {
    return {
      'id': id,
      'firstName': firstName,
      'lastName': lastName,
      'imageUrl': imageUrl,
      'userStatus': userStatus,
      'location': location,
      'isFirstLogin': isFirstLogin,
      'distance': distance,
    };
  }

  Map<String, dynamic> toJsonLibrary() {
    return {
      'id': id,
      'name': (firstName??'') + ' ' + (lastName??''),
      'imageUrl': imageUrl,
      'distance': distance,
      'address': address,
    };
  }

  @override
  String toString() {
    return 'User{id: $id, address: $address, userStatus: $userStatus, firstName: $firstName, lastName: $lastName, imageUrl: $imageUrl, gender: $gender, bookList: $bookList, location: $location, isFirstLogin: $isFirstLogin}';
  }
}
