// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_book.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LibraryBook _$LibraryBookFromJson(Map<String, dynamic> json) {
  return LibraryBook(
    libraryBookId: json['libraryBookId'] as String,
    libraryId: json['libraryId'] as String,
    status: json['status'] as String,
    book: json['book'] == null
        ? null
        : Book.fromJson(json['book'] as Map<String, dynamic>),
    request: json['request'] == null
        ? null
        : RequestDto.fromJson(json['request'] as Map<String, dynamic>),
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$LibraryBookToJson(LibraryBook instance) =>
    <String, dynamic>{
      'libraryBookId': instance.libraryBookId,
      'libraryId': instance.libraryId,
      'status': instance.status,
      'book': instance.book,
      'request': instance.request,
      'user': instance.user,
    };
