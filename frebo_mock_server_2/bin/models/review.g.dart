// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Review _$ReviewFromJson(Map<String, dynamic> json) {
  return Review(
    bookReviewId: json['bookReviewId'] as String,
    bookId: json['bookId'] as String,
    isEditable: json['isEditable'] as bool,
    review: json['review'] as String,
    reviewDate: json['reviewDate'] == null
        ? null
        : DateTime.parse(json['reviewDate'] as String),
    rating: json['rating'] as int,
    distance: json['distance'] as int,
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'bookReviewId': instance.bookReviewId,
      'bookId': instance.bookId,
      'isEditable': instance.isEditable,
      'review': instance.review,
      'rating': instance.rating,
      'distance': instance.distance,
      'user': instance.user,
      'reviewDate': instance.reviewDate?.toIso8601String(),
    };
