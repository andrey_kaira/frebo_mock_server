// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_id_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookIdDto _$BookIdDtoFromJson(Map<String, dynamic> json) {
  return BookIdDto(
    bookId: json['bookId'] as String,
  );
}

Map<String, dynamic> _$BookIdDtoToJson(BookIdDto instance) => <String, dynamic>{
      'bookId': instance.bookId,
    };
