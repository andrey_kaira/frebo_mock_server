// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_library_books.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddLibraryBooks _$AddLibraryBooksFromJson(Map<String, dynamic> json) {
  return AddLibraryBooks(
    libraryId: json['libraryId'] as String,
    bookId: json['bookId'] as String,
  );
}

Map<String, dynamic> _$AddLibraryBooksToJson(AddLibraryBooks instance) =>
    <String, dynamic>{
      'libraryId': instance.libraryId,
      'bookId': instance.bookId,
    };
