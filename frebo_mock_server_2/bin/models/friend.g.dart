// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Friend _$FriendFromJson(Map<String, dynamic> json) {
  return Friend(
    activeRequestStatus: json['activeRequestStatus'] as String,
    libraryBookId: json['libraryBookId'] as String,
    requestId: json['requestId'] as String,
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$FriendToJson(Friend instance) => <String, dynamic>{
      'activeRequestStatus': instance.activeRequestStatus,
      'libraryBookId': instance.libraryBookId,
      'requestId': instance.requestId,
      'user': instance.user,
    };
