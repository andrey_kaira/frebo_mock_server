// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'accept_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AcceptRequestModel _$AcceptRequestModelFromJson(Map<String, dynamic> json) {
  return AcceptRequestModel(
    requestId: json['requestId'] as String,
    pickupLocation: json['pickupLocation'] as String,
    scheduledPickup: json['scheduledPickup'] as String,
    pickupTime: json['pickupTime'] as String,
  );
}

Map<String, dynamic> _$AcceptRequestModelToJson(AcceptRequestModel instance) =>
    <String, dynamic>{
      'requestId': instance.requestId,
      'pickupLocation': instance.pickupLocation,
      'scheduledPickup': instance.scheduledPickup,
      'pickupTime': instance.pickupTime,
    };
