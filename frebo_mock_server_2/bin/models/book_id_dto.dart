import 'package:json_annotation/json_annotation.dart';

part 'book_id_dto.g.dart';

@JsonSerializable()
class BookIdDto {

	factory BookIdDto.fromJson(Map<String, dynamic> json) => _$BookIdDtoFromJson(json);
	Map<String, dynamic> toJson() => _$BookIdDtoToJson(this);
  final String bookId;

  BookIdDto({
    this.bookId,
  });
}
