import 'package:json_annotation/json_annotation.dart';
import 'book.dart';
import 'request.dart';
import 'user.dart';

part 'library_book.g.dart';

@JsonSerializable()
class LibraryBook {
  factory LibraryBook.fromJson(Map<String, dynamic> json) => _$LibraryBookFromJson(json);

  Map<String, dynamic> toJson() => _$LibraryBookToJson(this);

  final String libraryBookId;
  final String libraryId;
  String status;
  final Book book;
  RequestDto request;
  User user;

  LibraryBook({
    this.libraryBookId,
    this.libraryId,
    this.status,
    this.book,
    this.request,
    this.user,
  });

  Map<String, dynamic> toJsonData() {
    return {
      'libraryBookId': libraryBookId,
      'libraryId': libraryId,
      'request': request?.toJsonRequest(),
      'status': status,
      'book': book,
      'user': user.toJsonAuth(),
    };
  }
}

class LibraryBookStatus {
  static const available = 'available';
  static const loaned = 'loaned';
}
