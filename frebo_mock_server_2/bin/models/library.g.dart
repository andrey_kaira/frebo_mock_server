// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Library _$LibraryFromJson(Map<String, dynamic> json) {
  return Library(
    id: json['id'] as String,
    name: json['name'] as String,
    isUserLibrary: json['isUserLibrary'] as bool,
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$LibraryToJson(Library instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'isUserLibrary': instance.isUserLibrary,
      'user': instance.user,
      'status': instance.status,
    };
