import 'book.dart';

class SearchModel{
  int count;
  final Book book;

  SearchModel({this.count = 1, this.book});
}