import 'package:json_annotation/json_annotation.dart';
import 'user.dart';

part 'rating.g.dart';

@JsonSerializable()
class Rating {
  factory Rating.fromJson(Map<String, dynamic> json) => _$RatingFromJson(json);

  Map<String, dynamic> toJson() => _$RatingToJson(this);
  String ratingId;
  final String bookId;
  final int rating;
  User user;

  Rating({
    this.ratingId,
    this.bookId,
    this.rating,
    this.user,
  });
}
