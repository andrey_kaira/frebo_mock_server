// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'genre.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Genre _$GenreFromJson(Map<String, dynamic> json) {
  return Genre(
    id: json['id'] as String,
    locale: json['locale'] as String,
    name: json['name'] as String,
    desc: json['desc'] as String,
  );
}

Map<String, dynamic> _$GenreToJson(Genre instance) => <String, dynamic>{
      'id': instance.id,
      'locale': instance.locale,
      'name': instance.name,
      'desc': instance.desc,
    };
