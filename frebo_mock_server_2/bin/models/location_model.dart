import 'package:json_annotation/json_annotation.dart';

part 'location_model.g.dart';

@JsonSerializable()
class LocationModel {

	factory LocationModel.fromJson(Map<String, dynamic> json) => _$LocationModelFromJson(json);
	Map<String, dynamic> toJson() => _$LocationModelToJson(this);
  final double lat;
  final double lng;

  LocationModel({
    this.lat,
    this.lng,
  });
}
