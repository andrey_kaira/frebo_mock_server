import 'package:json_annotation/json_annotation.dart';
import 'user.dart';

part 'token.g.dart';

@JsonSerializable()
class Token {

	factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);
	Map<String, dynamic> toJson() => _$TokenToJson(this);
  final String token;
  User user;

  Token({
    this.token,
    this.user,
  });

  Map<String, dynamic> toJsonAuth(){
    return {
      'token' : token,
      'user' : user.toJsonAuth(),
    };
  }
}
