import 'package:json_annotation/json_annotation.dart';
import 'user.dart';

part 'review.g.dart';

@JsonSerializable()
class Review {

	factory Review.fromJson(Map<String, dynamic> json) => _$ReviewFromJson(json);
	Map<String, dynamic> toJson() => _$ReviewToJson(this);
	String bookReviewId;
	final String bookId;
	bool isEditable;
	String review;
	int rating;
  int distance;
  User user;
  DateTime reviewDate;

  Review({
    this.bookReviewId,
    this.bookId,
    this.isEditable = false,
    this.review,
    this.reviewDate,
    this.rating = 0,
    this.distance = 0,
    this.user,
  }) {
    reviewDate ??= DateTime.now();
  }


  Map<String, dynamic> toJsonLibrary() {
    return {
      'bookReviewId': bookReviewId,
      'isEditable': isEditable,
      'review': review,
      'rating': rating,
      'reviewDate': reviewDate.toString(),
      'user': user.toJsonLibrary(),
    };
  }
}
