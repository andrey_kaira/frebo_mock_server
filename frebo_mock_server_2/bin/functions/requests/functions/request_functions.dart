import 'dart:convert';
import 'dart:math';

import 'package:google_maps_utils/spherical_utils.dart';
import 'package:shelf/shelf.dart';
import 'package:uuid/uuid.dart';

import '../../../models/book_mini.dart';
import '../../../models/request.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/library_local_requests.dart';
import '../../../requests/local_requests/request_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';
import '../../shared/not_found_functions.dart';

Future<Response> requestFunctions({Map<String, String> headers, Map<String, dynamic> body, String method}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  switch (method) {
    case 'POST':
      final bookId = body['bookId'];
      final libraryBookId = body['libraryBookId'];

      if (checkParamIsEmpty(libraryBookId) && checkParamIsEmpty(bookId)) return Response(400, body: 'Library id is empty');

      if (!checkParamIsEmpty(libraryBookId)) {
        final libraryBook = (await LibrariesLocalStoreRequest.getLibrariesBook(userId: currentToken.user.id)).firstWhere(
          (element) => element.libraryBookId == libraryBookId,
          orElse: () => null,
        );
        if (libraryBook == null) return Response(404, body: 'Library not found!');
        final book = libraryBook.book;
        if (book == null) return Response(404, body: 'Book not found!');

        final request = RequestDto(
          requestId: Uuid().v4().substring(0, 8),
          recipient: await AuthLocalStoreRequest.getUser(libraryBook.user.id),
          requestDate: DateTime.now(),
          libraryBookId: libraryBook.libraryBookId,
          book: BookMini.fromJson(book.toJson()),
          sender: currentToken.user,
        );

        RequestLocalStoreRequest.add(request: request);
      }
      return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
    case 'GET':
      final requests = (await RequestLocalStoreRequest.get())
          .where((element) => element.recipient.id == currentToken.user.id || element.sender.id == currentToken.user.id)
          .toList();
      await Future.forEach(requests, (element) async {
        requests[requests.indexOf(element)].sender = (await AuthLocalStoreRequest.getUser(element.sender.id));
        requests[requests.indexOf(element)].sender.distance = 0;
        requests[requests.indexOf(element)].recipient = (await AuthLocalStoreRequest.getUser(element.recipient.id));
        final from =
            Point(requests[requests.indexOf(element)].sender.location?.lat ?? 0, requests[requests.indexOf(element)].sender.location?.lng ?? 0);
        final to =
            Point(requests[requests.indexOf(element)].recipient.location?.lat ?? 0, requests[requests.indexOf(element)].recipient.location?.lng ?? 0);
        if (currentToken.user.id == requests[requests.indexOf(element)].sender.id) {
          requests[requests.indexOf(element)].recipient.distance = (SphericalUtils.computeDistanceBetween(from, to)).round();
        } else {
          requests[requests.indexOf(element)].sender.distance = (SphericalUtils.computeDistanceBetween(from, to)).round();
        }
      });
      return Response.ok(jsonEncode(requests.map((e) => e.toJsonRequest()).toList()), headers: CurrentHeaders.getHeaders());
    default:
      return NotFoundFunctions.notFoundFunction();
  }
}
