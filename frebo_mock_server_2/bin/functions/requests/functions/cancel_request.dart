import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/request_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> cancelRequestFunctions({Map<String, String> headers, Map<String, String> queryParameters}) async {
  final token = headers[AuthKeys.token];
  final requestId = queryParameters[RequestKeys.requestId];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  if (checkParamIsEmpty(requestId)) return Response(400, body: 'Request id is empty');

  final currentToken = await AuthLocalStoreRequest.getToken(token);
  final requests = await RequestLocalStoreRequest.get();
  final request = requests.firstWhere((element) => element.requestId == requestId, orElse: () => null);
  if (request == null) return Response.notFound('Request id is empty');
  if (request.sender.id != currentToken.user.id && request.recipient.id != currentToken.user.id) return Response.forbidden('Access denied!');

  RequestLocalStoreRequest.cancel(requestId: requestId);

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
}
