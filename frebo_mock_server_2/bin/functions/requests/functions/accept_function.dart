import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/accept_request_model.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/request_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> acceptFunctions({Map<String, String> headers, dynamic body}) async {
  final token = headers[AuthKeys.token];
  final acceptRequestModel = AcceptRequestModel.fromJson(body);

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  if (checkParamIsEmpty(acceptRequestModel.requestId)) return Response(400, body: 'Request is empty');
  if (checkParamIsEmpty(acceptRequestModel.pickupTime)) return Response(400, body: 'Time is empty');
  if (checkParamIsEmpty(acceptRequestModel.pickupLocation)) return Response(400, body: 'Pickup location is empty');
  if (checkParamIsEmpty(acceptRequestModel.scheduledPickup)) return Response(400, body: 'When is empty');

  final currentToken = await AuthLocalStoreRequest.getToken(token);
  final requests = await RequestLocalStoreRequest.get();
  final request = requests.firstWhere((element) => element.requestId == acceptRequestModel.requestId, orElse: () => null);
  if (request == null) return Response.notFound('Request id is empty');
  if (request.sender.id != currentToken.user.id && request.recipient.id != currentToken.user.id) return Response.forbidden('Access denied!');

  RequestLocalStoreRequest.accept(requestId: acceptRequestModel.requestId);

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
}
