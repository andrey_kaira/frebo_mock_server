import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_keys.dart';
import '../shared/not_found_functions.dart';
import 'functions/accept_function.dart';
import 'functions/book_returned.dart';
import 'functions/book_taken_or_picked.dart';
import 'functions/cancel_request.dart';
import 'functions/request_functions.dart';

class RequestsManager {
  static Future<shelf.Response> functionsManager(
      List<String> path, {
        String method,
        Map<String, String> headers,
        dynamic body,
        Map<String, String> queryParameters,
      }) async {
    if (path.length < 2) {
      return await requestFunctions(headers: headers, body: body, method: method);
    }
    switch (path.last) {
      case ApiKeys.cancelRequest:
        return await cancelRequestFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.accept:
        return await acceptFunctions(headers: headers, body: body);
      case ApiKeys.bookTakenOrPicked:
        return await bookTakenOrPickedFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.bookReturned:
        return await bookReturnedFunctions(headers: headers, queryParameters: queryParameters);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
