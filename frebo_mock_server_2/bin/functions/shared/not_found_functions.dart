import 'package:shelf/shelf.dart';

class NotFoundFunctions {
  static Response notFoundFunction() {
    return Response.notFound('Functions not found!');
  }
}
