import 'package:shelf/shelf.dart' as shelf;

import '../../requests/local_requests/auth_local_requests.dart';

Future<shelf.Response> checkTokenFunction(String token) async {
  try {
    if (token == null || token == '') {
      return shelf.Response.forbidden('Token is null!');
    }
    token = token.replaceAll('Bearer ', '');
    final idToken = await AuthLocalStoreRequest.getToken(token);
    if (idToken == null) {
      return shelf.Response.forbidden('Token is not found');
    }
    //if (idToken.ttlToken.isBefore(DateTime.now())) {
    //  return shelf.Response.forbidden('Token is expired!');
    //}
  } catch (error) {
    print(error);
    return shelf.Response.forbidden('An unexpected error has occurred!');
  }
  return null;
}
