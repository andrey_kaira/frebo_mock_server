import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/search_model.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> searchForLibraryFunctions({Map<String, String> headers, Map<String, String> queryParameters}) async {
  final token = headers[AuthKeys.token];
  final q = queryParameters[BooksKeys.q];
  final count = int.tryParse(queryParameters[BooksKeys.count] ?? '') ?? 20;
  final offset = int.tryParse(queryParameters[BooksKeys.offset] ?? '') ?? 0;

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  if (checkParamIsEmpty(q)) return Response(400, body: 'Query is empty');

  if (q.replaceAll(' ', '').length < 3) {
    return Response.ok(jsonEncode([]), headers: CurrentHeaders.getHeaders());
  }

  final value = q
      .toLowerCase()
      .replaceAll(':', '')
      .replaceAll('.', '')
      .replaceAll('&', '')
      .replaceAll('/', '')
      .replaceAll(',', '')
      .replaceAll('-', '')
      .replaceAll('\\', '')
      .replaceAll('+', '')
      .replaceAll('*', '')
      .replaceAll('"', '')
      .replaceAll("'", '');
  final values = value.split(' ');

  var searchModels = <SearchModel>[];
  var books = await BooksLocalStoreRequest.getBooks(currentToken.user.id);
  books.forEach((book) {
    final names = book.name
        .toLowerCase()
        .replaceAll(':', '')
        .replaceAll('.', '')
        .replaceAll(',', '')
        .replaceAll('&', '')
        .replaceAll('/', '')
        .replaceAll('\\', '')
        .replaceAll('-', '')
        .replaceAll('+', '')
        .replaceAll('*', '')
        .replaceAll('"', '')
        .replaceAll("'", '')
        .split(' ');

    values.sublist(0, values.length - 1).forEach((str) {
      final index = names.indexOf(str);
      if (index != -1) {
        if (!searchModels.any((element) => element.book.bookId == book.bookId)) {
          searchModels.add(SearchModel(book: book, count: index < values.length && names[index] == values[index] ? 2 : 1));
        } else {
          searchModels.firstWhere((element) => index < values.length && element.book.bookId == book.bookId).count +=
          names[index] == values[index] ? 2 : 1;
        }
      }
    });
    if (names.any((element) => element.startsWith(values.last))) {
      if (!searchModels.any((element) => element.book.bookId == book.bookId)) {
        if(names.length>=values.length && names[values.length-1].startsWith(values.last)){
          searchModels.add(SearchModel(book: book, count: 2));
        } else {
          searchModels.add(SearchModel(book: book, count: 1));
        }
      } else {
        if(names.length>=values.length && names[values.length-1].startsWith(values.last)){
          searchModels.firstWhere((element) => element.book.bookId == book.bookId).count += 2;
        } else {
          searchModels.firstWhere((element) => element.book.bookId == book.bookId).count += 1;
        }

      }
    }
  });

  searchModels.removeWhere((element) => element.count < values.length - 1);
  searchModels.sort((a, b) => b.count.compareTo(a.count));

  if (offset > books.length) {
    return Response.ok(jsonEncode([]), headers: CurrentHeaders.getHeaders());
  }
  searchModels = searchModels.sublist(offset, count + offset < searchModels.length ? count + offset : searchModels.length);
  return Response.ok(jsonEncode(searchModels.map((e) => e.book).toList().map((e) => e.toJson()).toList()), headers: CurrentHeaders.getHeaders());
}
