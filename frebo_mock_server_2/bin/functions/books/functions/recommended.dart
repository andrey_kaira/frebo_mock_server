import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/genre.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> recommendedFunctions({Map<String, String> headers, Map<String, String> queryParameters}) async {
  final token = headers[AuthKeys.token];
  final lat = double.tryParse(queryParameters[BooksKeys.lat] ?? '');
  final lng = double.tryParse(queryParameters[BooksKeys.lng] ?? '');
  final count = int.tryParse(queryParameters[BooksKeys.count] ?? '') ?? 20;
  final offset = int.tryParse(queryParameters[BooksKeys.offset] ?? '') ?? 0;
  final distanceFromLocation = int.tryParse(queryParameters[BooksKeys.distanceFromLocation] ?? '');

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  if (lat == null) return Response(400, body: 'Lat is empty');
  if (lng == null) return Response(400, body: 'Lng is empty');
  if (distanceFromLocation == null) return Response(400, body: 'Distance from location is empty');

  final currentToken = await AuthLocalStoreRequest.getToken(token);
  var books = await BooksLocalStoreRequest.getBooks(currentToken.user.id);

  if (currentToken.user.bookList == null || currentToken.user.bookList.isEmpty) {
    books = books.sublist(offset, count + offset < books.length ? count + offset : books.length);
    return Response.ok(jsonEncode(books.map((e) => e.toJson()).toList()), headers: CurrentHeaders.getHeaders());
  }
  final genres = <Genre>[];
  currentToken.user.bookList.forEach((element) {
    final book = books.firstWhere((book) => element.bookId == book.bookId, orElse: () => null);
    genres.addAll(book?.genres ?? []);
  });

  books.sort((bookA, bookB) {
    var res = 1;
    final genres = <Genre>[];
    currentToken.user.bookList.forEach((element) {
      if (bookA.bookId == element.bookId) {
        res = 0;
      }
      genres.addAll(books.firstWhere((book) => book.bookId == element.bookId, orElse: () => null)?.genres??[]);
    });
    var bookAContain = 0;
    var bookBContain = 0;

    bookA.genres.forEach((element) {
      if (genres.firstWhere((genre) => genre.id == element.id, orElse: () => null) != null) {
        bookAContain++;
      }
    });
    bookB.genres.forEach((element) {
      if (genres.firstWhere((genre) => genre.id == element.id, orElse: () => null) != null) {
        bookBContain++;
      }
    });
    if (res == 0) return res;
    return bookAContain > bookBContain ? -1 : 1;
  });

  if (offset > books.length) {
    return Response.ok(jsonEncode([]), headers: CurrentHeaders.getHeaders());
  }
  books = books.sublist(offset, count + offset < books.length ? count + offset : books.length);

  return Response.ok(jsonEncode(books.map((e) => e.toJson()).toList()), headers: CurrentHeaders.getHeaders());
}
