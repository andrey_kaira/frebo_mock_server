import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/book.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> getReviewsBookFunctions({Map<String, String> headers, Book book}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);
  final reviews = await BooksLocalStoreRequest.getReviewBooks();
  if (reviews == null) return Response.ok(jsonEncode([]), headers: CurrentHeaders.getHeaders());

  reviews.removeWhere((element) => element.bookId != book.bookId);
  if (reviews.isEmpty) return Response.ok(jsonEncode([]), headers: CurrentHeaders.getHeaders());

  reviews.forEach((element) {
    element.isEditable = (currentToken.user.id == element.user.id);
  });
  return Response.ok(jsonEncode(reviews.map((e) => e.toJsonLibrary()).toList().reversed.toList()), headers: CurrentHeaders.getHeaders());
}
