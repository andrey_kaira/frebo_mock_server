import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/rating.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_param.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> ratingsFunctions({Map<String, String> headers, Map<String, dynamic> body}) async {
  final token = headers[AuthKeys.token];
  final rating = Rating.fromJson(body);

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  if (checkParamIsEmpty(rating.bookId)) return Response(400, body: 'Book id is empty!');
  if (rating.rating == null) return Response(400, body: 'Rating id is empty!');
  if (rating.rating < 0 || rating.rating > 10) return Response(400, body: 'The rating is incorrect!');

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  BooksLocalStoreRequest.addRatingBooks(rating: rating, user: currentToken.user);

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
}
