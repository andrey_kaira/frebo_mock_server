import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/review.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> reviewsFunctions({Map<String, String> headers, Map<String, String> queryParameters, Map<String, dynamic> body, String method}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  switch (method) {
    case 'POST':
      final review = Review.fromJson(body);
      review.user = currentToken.user;
      review.isEditable = false;
      BooksLocalStoreRequest.addReviewBooks(review: review);
      return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
    case 'PUT':
      final bookReviewId = body['bookReviewId'];
      final review = body['review'];
      final reviews = await BooksLocalStoreRequest.getReviewBooks();
      final currentReview = reviews.firstWhere((element) => element.bookReviewId == bookReviewId, orElse: () => null);
      if (currentReview == null) return Response.notFound('Book review id not found!');
      if (currentReview.user.id != currentToken.user.id) return Response.forbidden('Access denied!');
      BooksLocalStoreRequest.updateReviewBooks(bookReviewId: bookReviewId, review: review);
      return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
    case 'DELETE':
      final bookReviewId = queryParameters['bookReviewId'];

      final reviews = await BooksLocalStoreRequest.getReviewBooks();
      final currentReview = reviews.firstWhere((element) => element.bookReviewId == bookReviewId, orElse: () => null);
      if (currentReview == null) return Response.notFound('Book review id not found!');
      if (currentReview.user.id != currentToken.user.id) return Response.forbidden('Access denied!');

      BooksLocalStoreRequest.deleteReviewBooks(bookReviewId: bookReviewId);
      return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
  }

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
}
