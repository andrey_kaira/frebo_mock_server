import 'dart:convert';
import 'dart:math';

import 'package:shelf/shelf.dart';

import '../../../models/book_mini.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> searchBarCodeFunctions({Map<String, String> headers, Map<String, String> queryParameters}) async {
  final token = headers[AuthKeys.token];
  final barcode  = queryParameters[BooksKeys.barcode];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  if (barcode == null) return Response(400, body: 'Bar code is empty');
  final books = await BooksLocalStoreRequest.getBooks(currentToken.user.id);
  var book = books[Random().nextInt(books.length)].toJson();
  return Response.ok(jsonEncode(BookMini.fromJson(book).toJson()), headers: CurrentHeaders.getHeaders());
}
