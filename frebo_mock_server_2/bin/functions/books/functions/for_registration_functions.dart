import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/book_mini.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> forRegistrationFunctions({Map<String, String> headers, Map<String, String> queryParameters}) async {
  final token = headers[AuthKeys.token];
  final offset = int.tryParse(queryParameters[BooksKeys.offset]) ?? 0;
  final count = int.tryParse(queryParameters[BooksKeys.count]) ?? 20;

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  var books = (await BooksLocalStoreRequest.getBooks(currentToken.user.id));
  books = books.sublist(offset, count + offset < books.length ? count + offset : books.length);
  final booksMini = <BookMini>[];
  books.forEach((element) {
    booksMini.add(BookMini.fromJson(element.toJson()));
  });

  return Response.ok(jsonEncode(booksMini.map((e) => e.toJson()).toList()), headers: CurrentHeaders.getHeaders());
}
