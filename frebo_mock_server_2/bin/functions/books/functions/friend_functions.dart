import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/book.dart';
import '../../../models/friend.dart';
import '../../../models/library_book.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/library_local_requests.dart';
import '../../../requests/local_requests/request_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> friendsFunctions({Map<String, String> headers, Book book}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  var libraries = await LibrariesLocalStoreRequest.getLibrariesBook(userId: currentToken.user.id);

  libraries = libraries.where((lib) => lib.book.bookId == book.bookId).toList();
  if (libraries.isEmpty) return Response.ok(jsonEncode([]), headers: CurrentHeaders.getHeaders());
  final friends = <Friend>[];

  await Future.forEach(libraries.where((element) => element.book.bookId == book.bookId).toList(), (LibraryBook element) async {
    final requestTemps = (await RequestLocalStoreRequest.get())
            ?.where(
              (request) => request.recipient.id == element.user.id && request.sender.id == currentToken.user.id,
            )
            ?.toList() ??
        [];

    if (requestTemps.isNotEmpty) {
      requestTemps.forEach((requestTemp) {
        friends.add(Friend(
          activeRequestStatus: requestTemp.status,
          requestId: requestTemp.requestId,
          user: requestTemp.recipient,
          libraryBookId: element.libraryBookId,
        ));
      });
    } else {
      friends.add(Friend(user: element.user, libraryBookId: element.libraryBookId));
    }
  });

  return Response.ok(jsonEncode(friends.map((e) => e.toJsonRequest()).toList()), headers: CurrentHeaders.getHeaders());
}
