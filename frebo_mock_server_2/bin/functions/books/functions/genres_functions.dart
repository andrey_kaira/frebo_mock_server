import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/books_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> genresFunctions({Map<String, String> headers}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  return Response.ok(jsonEncode((await BooksLocalStoreRequest.getGenresBooks()).map((e) => e.toJson()).toList()), headers: CurrentHeaders.getHeaders());
}
