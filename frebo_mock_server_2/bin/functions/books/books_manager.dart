import 'dart:convert';

import 'package:shelf/shelf.dart' as shelf;

import '../../requests/local_requests/auth_local_requests.dart';
import '../../requests/local_requests/books_local_requests.dart';
import '../../res/api_keys.dart';
import '../../res/functions_keys.dart';
import '../shared/check_token_function.dart';
import '../shared/current_headers.dart';
import '../shared/not_found_functions.dart';
import 'functions/for_registration_functions.dart';
import 'functions/friend_functions.dart';
import 'functions/genres_functions.dart';
import 'functions/get_reviews_book_function.dart';
import 'functions/rattings_functions.dart';
import 'functions/recommended.dart';
import 'functions/reviews_functions.dart';
import 'functions/search.dart';
import 'functions/search_bar_code.dart';
import 'functions/search_for_library.dart';

class BooksManager {
  static Future<shelf.Response> functionsManager(
    List<String> path, {
    String method,
    Map<String, String> headers,
    Map<String, String> queryParameters,
    Map<String, dynamic> body,
  }) async {
    switch (path[1]) {
      case ApiKeys.forRegistration:
        return await forRegistrationFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.genres:
        return await genresFunctions(headers: headers);
      case ApiKeys.recommended:
        return await recommendedFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.search:
        return await searchFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.searchForLibrary:
        return await searchForLibraryFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.searchBarCode:
        return await searchBarCodeFunctions(headers: headers, queryParameters: queryParameters);
      case ApiKeys.reviews:
        return await reviewsFunctions(method: method, headers: headers, body: body, queryParameters: queryParameters);
      case ApiKeys.ratings:
        return await ratingsFunctions(headers: headers, body: body);
      default:
        final token = headers[AuthKeys.token];

        final checkToken = await checkTokenFunction(token);
        if (checkToken != null) return checkToken;

        final currentToken = await AuthLocalStoreRequest.getToken(token);
        final books = await BooksLocalStoreRequest.getBooks(currentToken.user.id);
        final book = books.firstWhere((element) => element.bookId == path[1], orElse: () => null);
        if (book == null) return NotFoundFunctions.notFoundFunction();
        if(path.length<3) return shelf.Response.ok(jsonEncode(book.toJson()), headers: CurrentHeaders.getHeaders());
        switch(path[2]){
          case ApiKeys.reviews:
            return await getReviewsBookFunctions(headers: headers, book: book);
          case ApiKeys.friends:
            return await friendsFunctions(headers: headers, book: book);
          default:
            return NotFoundFunctions.notFoundFunction();
        }
    }
  }
}
