import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/token.dart';
import '../../../models/user.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../../service/keygen_service.dart';
import '../../shared/check_param.dart';
import '../../shared/current_headers.dart';

Future<Response> loginFunction({Map<String, String> headers, Map<String, dynamic> body}) async {
  final firebaseToken = headers[AuthKeys.firebaseToken];

  if (checkParamIsEmpty(firebaseToken)) return Response(400, body: 'Firebase token is empty');

  var currentUser = await AuthLocalStoreRequest.getUser(firebaseToken);
  currentUser ??= await AuthLocalStoreRequest.saveNewUser(
    User(
      id: firebaseToken,
      firstName: 'Test',
      lastName: 'test',
      isFirstLogin: true,
      userStatus: 0,
    ),
  );

  currentUser.isFirstLogin = true;

  final token = Token(user: currentUser, token: KeyGenService.long());

  AuthLocalStoreRequest.saveNewToken(token);

  return Response.ok(jsonEncode(token.toJsonAuth()), headers: CurrentHeaders.getHeaders());
}
