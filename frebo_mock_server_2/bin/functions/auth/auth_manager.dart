import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_keys.dart';
import '../shared/not_found_functions.dart';
import 'functions/login_function.dart';
import 'functions/logout.dart';

class AuthManager {
  static Future<shelf.Response> functionsManager(List<String> path, {Map<String, String> headers, Map<String, dynamic> body}) async {
    switch (path[1]) {
      case ApiKeys.login:
        return await loginFunction(body: body, headers: headers);
      case ApiKeys.logout:
        return await logoutFunction(headers: headers);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
