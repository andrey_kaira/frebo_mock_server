import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_keys.dart';
import '../shared/not_found_functions.dart';
import 'functions/users_functions.dart';

class UsersManager {
  static Future<shelf.Response> functionsManager(List<String> path, {String method, Map<String, String> headers, Map<String, dynamic> body}) async {
    switch (path.first) {
      case ApiKeys.users:
        return await usersFunctions(headers: headers, body: body, method: method);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
