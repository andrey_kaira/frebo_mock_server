import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../models/user.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> usersFunctions({Map<String, String> headers, Map<String, dynamic> body, String method}) async {
    final token = headers[AuthKeys.token];

    final checkToken = await checkTokenFunction(token);
    if (checkToken != null) return checkToken;

    final currentToken = await AuthLocalStoreRequest.getToken(token);

    switch (method) {
      case 'GET':
        return Response.ok(jsonEncode(currentToken.user.toJsonAuth()), headers: CurrentHeaders.getHeaders());
      case 'PUT':
        print(body);
        final userDetail = User.fromJson(body);
        final user = currentToken.user.copyWith(user: userDetail);
        AuthLocalStoreRequest.updateUser(user);
        AuthLocalStoreRequest.updateTokenUser(user);
        return Response.ok(jsonEncode({'status':'Ok!'}), headers: CurrentHeaders.getHeaders());
      case 'DELETE':
        AuthLocalStoreRequest.deleteUser(currentToken.user);
        AuthLocalStoreRequest.removeTokenUser(currentToken);
        return Response.ok(jsonEncode({'status':'Ok!'}), headers: CurrentHeaders.getHeaders());
  }

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());

}
