import 'dart:convert';

import 'package:shelf/shelf.dart';
import 'package:uuid/uuid.dart';

import '../../../models/add_library_books.dart';
import '../../../models/library_book.dart';
import '../../../models/request.dart';
import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/books_local_requests.dart';
import '../../../requests/local_requests/library_local_requests.dart';
import '../../../requests/local_requests/request_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';

Future<Response> libraryBooksFunctions({Map<String, String> headers, Map<String, String> queryParameters, dynamic body, String method}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  switch (method) {
    case 'GET':
      final userId = queryParameters['userId'];
      final libraries = (await LibrariesLocalStoreRequest.getLibrariesBook(userId: currentToken.user.id)).where((element) {
        return element.user.id == userId;
      }).toList();

      //await Future.forEach(libraries, (Library lib) async {
      //  lib.books = allBooks.where((book) => lib.books.any((element) => element.bookId == book.bookId)).toList();
      //});
      await Future.forEach(libraries, (LibraryBook lib) async {
        lib.request =
            (await RequestLocalStoreRequest.get()).firstWhere(
              (element) => lib.libraryBookId == element.libraryBookId  && element.status == RequestStatus.bookTaken,
              orElse: () => null,
            );
        if (lib.request != null) {
          lib.request.sender = await AuthLocalStoreRequest.getUser(lib.request.sender.id);
          lib.request.recipient = await AuthLocalStoreRequest.getUser(lib.request.recipient.id);
          lib.status = LibraryBookStatus.loaned;
        } else {
          lib.status = LibraryBookStatus.available;
        }
      });

      return Response.ok(
        jsonEncode(libraries.map((e) => e.toJsonData()).toList()),
        headers: CurrentHeaders.getHeaders(),
      );
    case 'DELETE':
      final libraryBookId = body['libraryBookId'];
      final library =
          (await LibrariesLocalStoreRequest.getLibrariesBook(userId: currentToken.user.id)).firstWhere((element) => element.libraryBookId == libraryBookId, orElse: () => null);
      if (library == null) return Response(400, body: 'Library id incorrectly!');
      LibrariesLocalStoreRequest.deletedBookLibrary(libraryBookId: libraryBookId);
      return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
    case 'POST':
      try {
        final addLibraryBooks = List<AddLibraryBooks>.unmodifiable(body.map((e) => AddLibraryBooks.fromJson(e)).toList());
        final books = (await BooksLocalStoreRequest.getBooks(currentToken.user.id))
            .where((element) => addLibraryBooks.any((book) => book.bookId == element.bookId))
            .toList();
        if (books.isEmpty) return Response(400, body: 'Books is empty!');
        final library = (await LibrariesLocalStoreRequest.getLibraries()).firstWhere(
          (element) => addLibraryBooks.any((lib) => element.id == lib.libraryId && currentToken.user.id == element.user.id),
          orElse: () => null,
        );
        if (library == null) return Response(404, body: 'Library is not found!');
        if (library.user.id != currentToken.user.id) return Response(403, body: 'You cannot add another user to your library!');

        LibrariesLocalStoreRequest.addBooksLibrary(
            libraryBooks: List<LibraryBook>.unmodifiable(addLibraryBooks
                .map(
                  (e) => LibraryBook(
                    libraryBookId: Uuid().v4(),
                    user: currentToken.user,
                    libraryId: e.libraryId,
                    book: books.firstWhere((book) => e.bookId == book.bookId),
                  ),
                )
                .toList()));
        return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
      } catch (error) {
        print(error);
        return Response(400, body: 'Error body!');
      }
  }

  return Response.ok(jsonEncode({'status': 'Ok!'}), headers: CurrentHeaders.getHeaders());
}
