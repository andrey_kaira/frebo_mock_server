import 'dart:convert';

import 'package:shelf/shelf.dart';

import '../../../requests/local_requests/auth_local_requests.dart';
import '../../../requests/local_requests/library_local_requests.dart';
import '../../../res/functions_keys.dart';
import '../../shared/check_token_function.dart';
import '../../shared/current_headers.dart';
import '../../shared/not_found_functions.dart';

Future<Response> libraryFunctions({Map<String, String> headers, Map<String, dynamic> body, String method}) async {
  final token = headers[AuthKeys.token];

  final checkToken = await checkTokenFunction(token);
  if (checkToken != null) return checkToken;

  final currentToken = await AuthLocalStoreRequest.getToken(token);

  switch (method) {
    case 'GET':
      final libraryList = await LibrariesLocalStoreRequest.getLibraries();
      final library = libraryList.where((element) => element.user.id == currentToken.user.id).toList();
      final user = await AuthLocalStoreRequest.getUser(library.first.user.id);
      return Response.ok(
        jsonEncode(library.map((e) {
          e.isUserLibrary = true;
          e.user = user;
          return e.toJsonLibrary();
        }).toList()),
        headers: CurrentHeaders.getHeaders(),
      );
    default:
      return NotFoundFunctions.notFoundFunction();
  }
}
