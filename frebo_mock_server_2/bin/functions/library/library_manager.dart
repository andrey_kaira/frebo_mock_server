import 'package:shelf/shelf.dart' as shelf;

import '../../res/api_keys.dart';
import '../shared/not_found_functions.dart';
import 'functions/library_books_functions.dart';
import 'functions/library_functions.dart';

class LibraryManager {
  static Future<shelf.Response> functionsManager(
    List<String> path, {
    String method,
    Map<String, String> headers,
    dynamic body,
    Map<String, String> queryParameters,
  }) async {
    if (path.length < 2) {
      return await libraryFunctions(headers: headers, body: body, method: method);
    }
    switch (path.last) {
      case ApiKeys.books:
        return await libraryBooksFunctions(headers: headers, body: body, method: method, queryParameters: queryParameters);
      default:
        return NotFoundFunctions.notFoundFunction();
    }
  }
}
