import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:html/parser.dart' show parse;
import 'package:html/dom.dart';
import 'package:uuid/uuid.dart';
import '../models/book.dart';
import '../models/genre.dart';
import '../requests/local_requests/books_local_requests.dart';

class BooksService {
  static String _currentOLId;
  static final Uuid _uuid = Uuid();



  static void fillCountable(List<Book> books, List<Genre> genres, int count) async {
    var i = books.length;
    var j = genres.length;
    while (books.length < count) {
      await _fillOne(books, genres);
      await Future.delayed(Duration(seconds: 1));
      if (i < books.length) {
        BooksLocalStoreRequest.addBooks(book: books.last);
        i++;
      }
      if (j < genres.length) {
        BooksLocalStoreRequest.addGenres(genres.last);
        j++;
      }
    }
  }

  static Future<void> _fillOne(List<Book> books, List<Genre> genres) async {
    final newBook = Book();
    Map<String, dynamic> rez;

    try {
      rez = await _getOLMap(await _getRandomBookLink());
    } catch (e) {
      return;
    }

    if (!rez.containsKey('isbn_13')) {
      return;
    }
    if (rez.containsKey('description')) {
      if (rez['description'] is String) {
        newBook.summary = rez['description'];
      } else {
        newBook.summary = rez['description']['value'];
      }
    }
    if (rez.containsKey('first_sentence') && newBook.summary == null) {
      if (rez['first_sentence'] is String) {
        newBook.summary = rez['first_sentence'];
      } else {
        newBook.summary = rez['first_sentence']['value'];
      }
    }
    newBook.bookId = rez['isbn_13'][0];

    if(books.any((element) => element.bookId == newBook.bookId)){
      return;
    }

    try {
      rez = await _getISBNMap(rez['isbn_13'][0]);
    } catch (e) {
      return;
    }



    dynamic data;
    try {
      data = rez['records']['/books/$_currentOLId']['data'];
    } catch (e) {
      return;
    }

    if (!data.containsKey('title') || !data.containsKey('cover') || !data.containsKey('subjects')) {
      return;
    }

    newBook.name = data['title'];
    newBook.author = data.containsKey('authors') ? data['authors'][0]['name'] : null;
    newBook.coverImageUrl = data['cover']['large'];
    newBook.genres =
        data['subjects'].map<Genre>((item) => Genre(id: _uuid.v4(), locale: 'en', name: item['name'], desc: 'Description placeholder')).toList();

    for (var item in newBook.genres) {
      if (!genres.any((i) => i.name == item.name)) {
        genres.add(item);
      }
    }
    books.add(newBook);
  }

  static Future<String> _getRandomBookLink() async {
    Document doc;
    try {
      doc = parse((await http.get('https://openlibrary.org/random').catchError((error) => print(error.toString()))).body);

      return (doc.getElementsByTagName('link')[0]).attributes['href'].toString();
    } catch (e) {
      print(e.toString());
    }
  }

  static Future<Map<String, dynamic>> _getOLMap(String ref) async {
    ref = ref.substring(0, ref.lastIndexOf('/')) + '.json';
    _currentOLId = ref.substring(ref.lastIndexOf('/') + 1, ref.lastIndexOf('.'));
    return jsonDecode((await http.get(ref)).body);
  }

  static Future<Map<String, dynamic>> _getISBNMap(String isbn13) async {
    var temp = jsonDecode((await http.get('http://openlibrary.org/api/volumes/brief/isbn/' + isbn13 + '.json')).body);

    return temp;
  }
}
