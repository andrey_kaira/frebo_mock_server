import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

import '../models/book.dart';
import '../models/genre.dart';

class ParseService {
  static Future<bool> getBooks({List<Book> books, List<Genre> genres, int offset}) async {
    var isEnd = false;
    var headers = {
      'Content-Type': 'application/json',
      'Authorization':
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InZpamF5ZW5kcmFwYWdhcmUwNUBnbWFpbC5jb20iLCJ1c2VyaWQiOiI1ZDVmZWYyM2ZmOGIxYzQ1ZDA3ZTcwYTEiLCJpYXQiOjE1NjY2ODIyOTAsImV4cCI6MTU2NjY4NTg5MH0.7uHXgn8dHujCDy9WPOKNEQOLTM5lEFTUdKPNonYBw5Y',
      'x-rapidapi-key': 'feb8a1b322mshf9542550f36b400p12e3cfjsn11d3dc1a22c6'
    };
    var request = http.Request('GET', Uri.parse('https://www.googleapis.com/books/v1/volumes?q=_&maxResults=40&startIndex=$offset&langRestrict=en'));
    request.body = '''''';
    request.headers.addAll(headers);

    final response = await request.send();

    if (response.statusCode == 200) {
      final res = jsonDecode(await response.stream.bytesToString());
      if (res['totalItems'] < offset) {
        print('${res['totalItems']} + ${(offset + 40)}');
        isEnd = true;
      }
      final List<dynamic> items = res['items'];
      if (items == null) {
        return true;
      }
      items.forEach((element) {
        final categories = element['volumeInfo']['categories'];
        final id = element['id'];
        final title = element['volumeInfo']['title'];
        final imageLinks = element['volumeInfo']['imageLinks'];
        var image;
        if (imageLinks != null) image = imageLinks['smallThumbnail'];
        final author = element['volumeInfo']['authors']?.first;
        final dynamicGenre = <Genre>[];
        if (categories != null) {
          categories.forEach((category) {
            var genre = genres.firstWhere((element) => element.name == category, orElse: () => null);
            if (genre == null) {
              genre = Genre(id: Uuid().v4().substring(0, 8), name: category);
              genres.add(genre);
            }
            dynamicGenre.add(genre);
          });
        }
        books.add(
          Book(
            bookId: id,
            name: title,
            coverImageUrl: image,
            author: author,
            genres: dynamicGenre,
          ),
        );
      });
    } else {
      print(response.reasonPhrase);
      isEnd = true;
    }
    return isEnd;
  }
}
