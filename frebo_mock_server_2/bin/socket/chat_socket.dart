import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart' as http;

import '../functions/shared/check_param.dart';
import '../functions/shared/check_token_function.dart';

class ChatSocket {
  static ServerSocket _locationSocket;

  static Future<void> init({String ip, int port}) async {
    _locationSocket = await ServerSocket.bind(ip, port);
    await _locationSocket.listen((client) => _handleConnection(client));
    print('ChatSocket start $ip:$port');
  }

  static void _handleConnection(Socket client) {
    try {
      print('Connection from' ' ${client.remoteAddress.address}:${client.remotePort}');
      client.listen(
            (Uint8List data) async {
          try {
            final message = Utf8Codec().decode(data);
            final body = jsonDecode(message);

            client.write(jsonEncode([]));
          } catch (error) {
            print(error);
          }
        },
        onError: (error) {
          try {
            print('$error');
            client.close();
            client.destroy();
          } catch (error) {
            print('Error? $error');
          }
        },
        onDone: () {
          try {
            print('${client.address.address}:${client.remotePort} disconnect! (Done)');
            client.close();
          } catch (error) {
            print(error);
          }
        },
        cancelOnError: true,
      );
    } catch (error) {
      print(error);
    }
  }
}
