import 'dart:convert';
import 'dart:io';

import '../../models/library.dart';
import '../../models/library_book.dart';
import 'auth_local_requests.dart';
import 'books_local_requests.dart';

class LibrariesLocalStoreRequest {
  static void addLibraries({Library library}) async {
    var title = 'libraries';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([library.toJson()]));
    } else {
      final libraries = <Library>[];
      jsonDecode(body).forEach((element) {
        libraries.add(Library.fromJson(element));
      });
      libraries.add(library);
      await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
    }
  }

  static void deletedBookLibrary({String libraryBookId}) async {
    var title = 'library_books';

    final file = await _getFile(title);
    final body = await file.readAsString();

    var libraries = <LibraryBook>[];
    jsonDecode(body).forEach((element) {
      libraries.add(LibraryBook.fromJson(element));
    });

    libraries = libraries.where((element) => element.libraryBookId != libraryBookId).toList();

    await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
  }

  static Future<List<LibraryBook>> getLibrariesBook({String userId}) async {
    var title = 'library_books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return [];
    } else {
      final libraries = <LibraryBook>[];
      await Future.forEach(jsonDecode(body), (element) async {
        final library = LibraryBook.fromJson(element);
        library.user = await AuthLocalStoreRequest.getUser(library.user.id);
        libraries.add(library);
      });
      final ratings = await BooksLocalStoreRequest.getRatingsBooks();
      if (ratings != null && ratings.isNotEmpty) {
        libraries.forEach((lib) {
          final selectRatings = ratings.where((element) => element.bookId == lib.book.bookId);
          if (selectRatings.isNotEmpty) {
            lib.book.ratingCount = selectRatings.length;
            var sum = 0.0;
            selectRatings.forEach((element) {
              sum += element.rating;
            });
            lib.book.rating = (sum / selectRatings.length).round();
            if (userId != null) {
              final myRating = selectRatings.firstWhere((element) => element.user.id == userId, orElse: () => null);
              lib.book.myRating = myRating?.rating ?? 0;
            }
          } else {
            lib.book.ratingCount = 0;
            lib.book.rating = 0;
          }
        });
      }
      return libraries;
    }
  }

  static Future<List<Library>> getLibraries() async {
    var title = 'libraries';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final libraries = <Library>[];
      await Future.forEach(jsonDecode(body), (element) async {
        final library = Library.fromJson(element);
        library.user = await AuthLocalStoreRequest.getUser(library.user.id);
        libraries.add(library);
      });
      return libraries;
    }
  }

  static void addBooksLibrary({List<LibraryBook> libraryBooks}) async {
    var title = 'library_books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    final libraries = <LibraryBook>[];
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode(libraryBooks.map((e) => e.toJson()).toList()));
    } else {
      jsonDecode(body).forEach((element) {
        libraries.add(LibraryBook.fromJson(element));
      });
      libraries.addAll(libraryBooks);
      await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
    }
  }

  static Future<File> _getFile(title) async {
    File file;
    if (!await File('../$title.txt').exists()) {
      file = await File('../$title.txt').create();
      print('Create file ' + file.path);
    } else {
      file = await File('../$title.txt');
    }
    return file;
  }
}
