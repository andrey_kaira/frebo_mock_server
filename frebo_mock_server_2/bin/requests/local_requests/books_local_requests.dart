import 'dart:convert';
import 'dart:io';

import 'package:uuid/uuid.dart';

import '../../models/book.dart';
import '../../models/genre.dart';
import '../../models/rating.dart';
import '../../models/review.dart';
import '../../models/user.dart';

class BooksLocalStoreRequest {
  static void saveBooks() async {
    var title = 'books';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final books = (await getBooks(null)).sublist(0, 200);
    final genres = <Genre>[];
    var offset = 0;

   // BooksService.fillCountable(books, genres, 300);
    await file.writeAsString(jsonEncode(books.map((e) => e.toJson()).toList()));
  }

  static void addBooks({Book book}) async {
    var title = 'books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([book.toJson()]));
    } else {
      final books = <Book>[];
      jsonDecode(body).forEach((element) {
        books.add(Book.fromJson(element));
      });
      books.add(book);
      await file.writeAsString(jsonEncode(books.map((e) => e.toJson()).toList()));
    }
  }

  static void addReviewBooks({Review review}) async {
    var title = 'reviews';

    final file = await _getFile(title);
    final body = await file.readAsString();
    review.bookReviewId = Uuid().v4().substring(0, 8);
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([review.toJson()]));
    } else {
      final reviews = <Review>[];
      jsonDecode(body).forEach((element) {
        reviews.add(Review.fromJson(element));
      });
      reviews.add(review);
      await file.writeAsString(jsonEncode(reviews.map((e) => e.toJson()).toList()));
    }
  }

  static void addRatingBooks({Rating rating, User user}) async {
    var title = 'ratings';

    final file = await _getFile(title);
    final body = await file.readAsString();
    rating.ratingId = rating.ratingId ?? Uuid().v4().substring(0, 8);
    rating.user = user;
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([rating.toJson()]));
    } else {
      final ratings = <Rating>[];
      jsonDecode(body).forEach((element) {
        ratings.add(Rating.fromJson(element));
      });
      ratings.removeWhere((element) => element.user.id == user.id && element.bookId == rating.bookId);
      ratings.add(rating);
      await file.writeAsString(jsonEncode(ratings.map((e) => e.toJson()).toList()));
    }
  }

  static void updateReviewBooks({String bookReviewId, String review}) async {
    var title = 'reviews';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final reviews = <Review>[];
    jsonDecode(body).forEach((element) {
      reviews.add(Review.fromJson(element));
    });
    reviews.firstWhere((element) => element.bookReviewId == bookReviewId).review = review;
    await file.writeAsString(jsonEncode(reviews.map((e) => e.toJson()).toList()));
  }

  static void deleteReviewBooks({String bookReviewId}) async {
    var title = 'reviews';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final reviews = <Review>[];
    jsonDecode(body).forEach((element) {
      reviews.add(Review.fromJson(element));
    });
    reviews.removeWhere((element) => element.bookReviewId == bookReviewId);
    await file.writeAsString(jsonEncode(reviews.map((e) => e.toJson()).toList()));
  }

  static Future<List<Review>> getReviewBooks() async {
    var title = 'reviews';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final reviews = <Review>[];
      await Future.forEach(jsonDecode(body), (element) async {
        final review = Review.fromJson(element);
        final ratings = await getRatingsBooks();
        if (ratings != null && ratings.isNotEmpty) {
          review.rating =
              ratings.firstWhere((element) => element.bookId == review.bookId && review.user.id == element.user.id, orElse: () => null)?.rating;
        }
        reviews.add(review);
      });
      return reviews;
    }
  }

  static Future<List<Rating>> getRatingsBooks() async {
    var title = 'ratings';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final ratings = <Rating>[];
      jsonDecode(body).forEach((element) {
        ratings.add(Rating.fromJson(element));
      });
      return ratings;
    }
  }

  static Future<List<Genre>> getGenresBooks() async {
    var title = 'genres';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return null;
    } else {
      final genres = <Genre>[];
      jsonDecode(body).forEach((element) {
        genres.add(Genre.fromJson(element));
      });
      return genres;
    }
  }

  static void addGenres(Genre genre) async {
    var title = 'genres';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([genre.toJson()]));
    } else {
      final genres = <Genre>[];
      jsonDecode(body).forEach((element) {
        genres.add(Genre.fromJson(element));
      });
      genres.add(genre);
      await file.writeAsString(jsonEncode(genres.map((e) => e.toJson()).toList()));
    }
  }

  static void saveGenres(List<Genre> genres) async {
    var title = 'genres';

    final file = await _getFile(title);
    await file.writeAsString(jsonEncode(genres.map((e) => e.toJson()).toList()));
  }

  static Future<List<Book>> getBooks(String userId) async {
    var title = 'books';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return [];
    } else {
      final books = <Book>[];
      jsonDecode(body).forEach((element) {
        books.add(Book.fromJson(element));
      });
      final ratings = await getRatingsBooks();
      if (ratings != null && ratings.isNotEmpty) {
        books.forEach((book) {
          final selectRatings = ratings.where((element) => element.bookId == book.bookId);
          if (selectRatings.isNotEmpty) {
            book.ratingCount = selectRatings.length;
            var sum = 0.0;
            selectRatings.forEach((element) {
              sum += element.rating;
            });
            book.rating = (sum / selectRatings.length).round();
            if (userId != null) {
              final myRating = selectRatings.firstWhere((element) => element.user.id == userId, orElse: () => null);
              book.myRating = myRating?.rating ?? 0;
            }
          } else {
            book.ratingCount = 0;
            book.rating = 0;
          }
        });
      }
      return books;
    }
  }

  static Future<File> _getFile(title) async {
    File file;
    if (!await File('../$title.txt').exists()) {
      file = await File('../$title.txt').create();
      print('Create file ' + file.path);
    } else {
      file = await File('../$title.txt');
    }
    return file;
  }
}
