import 'dart:convert';
import 'dart:io';

import '../../models/request.dart';

class RequestLocalStoreRequest {
  static void add({RequestDto request}) async {
    var title = 'requests';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body == '') {
      await file.writeAsString(jsonEncode([request.toJson()]));
    } else {
      final requests = <RequestDto>[];
      jsonDecode(body).forEach((element) {
        requests.add(RequestDto.fromJson(element));
      });
      requests.add(request);
      await file.writeAsString(jsonEncode(requests.map((e) => e.toJson()).toList()));
    }
  }

  static void cancel({String requestId}) async {
    var title = 'requests';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final libraries = <RequestDto>[];
    jsonDecode(body).forEach((element) {
      libraries.add(RequestDto.fromJson(element));
    });
    libraries.firstWhere((element) => element.requestId == requestId).status = RequestStatus.cancelled;

    await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
  }

  static void accept({String requestId}) async {
    var title = 'requests';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final libraries = <RequestDto>[];
    jsonDecode(body).forEach((element) {
      libraries.add(RequestDto.fromJson(element));
    });
    libraries.firstWhere((element) => element.requestId == requestId).status = RequestStatus.awaitingDelivery;

    await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
  }

  static void bookTakenOrPicked({String requestId}) async {
    var title = 'requests';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final libraries = <RequestDto>[];
    jsonDecode(body).forEach((element) {
      libraries.add(RequestDto.fromJson(element));
    });
    libraries.firstWhere((element) => element.requestId == requestId).status = RequestStatus.bookTaken;
    libraries.firstWhere((element) => element.requestId == requestId).scheduledDelivery = DateTime.now();

    await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
  }

  static void bookReturned({String requestId}) async {
    var title = 'requests';

    final file = await _getFile(title);
    final body = await file.readAsString();

    final libraries = <RequestDto>[];
    jsonDecode(body).forEach((element) {
      libraries.add(RequestDto.fromJson(element));
    });
    libraries.firstWhere((element) => element.requestId == requestId).status = RequestStatus.bookReturned;
    libraries.firstWhere((element) => element.requestId == requestId).scheduledReturn = DateTime.now();

    await file.writeAsString(jsonEncode(libraries.map((e) => e.toJson()).toList()));
  }

  static Future<List<RequestDto>> get() async {
    var title = 'requests';

    final file = await _getFile(title);
    final body = await file.readAsString();
    if (body == null || body.isEmpty) {
      return [];
    } else {
      final requests = <RequestDto>[];
      jsonDecode(body).forEach((element) {
        requests.add(RequestDto.fromJson(element));
      });
      return requests;
    }
  }

  static Future<File> _getFile(title) async {
    File file;
    if (!await File('../$title.txt').exists()) {
      file = await File('../$title.txt').create();
      print('Create file ' + file.path);
    } else {
      file = await File('../$title.txt');
    }
    return file;
  }
}
