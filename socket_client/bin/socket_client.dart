import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

void main() async {
  // connect to the socket server
  final socket = await Socket.connect('165.232.155.214', 4568);
  print('Connected to: ${socket.remoteAddress.address}:${socket.remotePort}');

  // listen for responses from the server
  socket.listen(
    // handle data from the server
    (Uint8List data) {
      final serverResponse = Utf8Codec().decode(data);
      print('Server: $serverResponse');
    },

    // handle errors
    onError: (error) {
      print(error);
      socket.destroy();
    },

    // handle server ending connection
    onDone: () {
      print('Server left.');
      socket.destroy();
    },
    cancelOnError: false,
  );

  // send some messages to the server
  await sendMessage(socket, jsonEncode({'token': '58650059-eaf9-4260-a8b2-d7da6f4d9c42', 'value': 'ירושלים'}));
  await sendMessage(socket, jsonEncode({'token': '58650059-eaf9-4260-a8b2-d7da6f4d9c42', 'value': 'The Body', 'mode': 'library'}));
  await sendMessage(socket, jsonEncode({'token': '58650059-eaf9-4260-a8b2-d7da6f4d9c42', 'value': 'Me: Elton John Official Autobiography', 'mode': 'library'}));
  await Future.delayed(Duration(seconds: 2));
  await socket.close();
}

Future<void> sendMessage(Socket socket, String message) async {
  print('Client: $message');
  socket.write(message);
  await Future.delayed(Duration(seconds: 2));
}
